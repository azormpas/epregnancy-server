CREATE DATABASE  IF NOT EXISTS `ebabycom_ebabyapp` DEFAULT CHARACTER SET utf8 DEFAULT COLLATE utf8_general_ci;
USE `ebabycom_ebabyapp`;
--
-- Table structure for table `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `name` varchar(255),
  `surname` varchar(255),
  `residence_town` varchar(45) ,
  `email` varchar(255) ,
  `conceptualdate` date,
  `password` varchar(255) ,
  `user_photo` varchar(255) ,
  `created_at` timestamp DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `email_UNIQUE` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8; 


--
-- Table structure for table gynecological_tests
--

CREATE TABLE IF NOT EXISTS  `week` (
    `id` int  NOT NULL AUTO_INCREMENT,
    `week` int NOT NULL,
    PRIMARY KEY (`id`)
)ENGINE=InnoDB DEFAULT CHARSET=utf8; 
--
-- Initialize for table week
--
INSERT INTO `week` (`id`, `week`) VALUES ('1',1);
INSERT INTO `week` (`id`, `week`) VALUES ('2',2);
INSERT INTO `week` (`id`, `week`) VALUES ('3',3);
INSERT INTO `week` (`id`, `week`) VALUES ('4',4);
INSERT INTO `week` (`id`, `week`) VALUES ('5',5);
INSERT INTO `week` (`id`, `week`) VALUES ('6',6);
INSERT INTO `week` (`id`, `week`) VALUES ('7',7);
INSERT INTO `week` (`id`, `week`) VALUES ('8',8);
INSERT INTO `week` (`id`, `week`) VALUES ('9',9);
INSERT INTO `week` (`id`, `week`) VALUES ('10',10);
INSERT INTO `week` (`id`, `week`) VALUES ('11',11);
INSERT INTO `week` (`id`, `week`) VALUES ('12',12);
INSERT INTO `week` (`id`, `week`) VALUES ('13',13);
INSERT INTO `week` (`id`, `week`) VALUES ('14',14);
INSERT INTO `week` (`id`, `week`) VALUES ('15',15);
INSERT INTO `week` (`id`, `week`) VALUES ('16',16);
INSERT INTO `week` (`id`, `week`) VALUES ('17',17);
INSERT INTO `week` (`id`, `week`) VALUES ('18',18);
INSERT INTO `week` (`id`, `week`) VALUES ('19',19);
INSERT INTO `week` (`id`, `week`) VALUES ('20',20);
INSERT INTO `week` (`id`, `week`) VALUES ('21',21);
INSERT INTO `week` (`id`, `week`) VALUES ('22',22);
INSERT INTO `week` (`id`, `week`) VALUES ('23',23);
INSERT INTO `week` (`id`, `week`) VALUES ('24',24);
INSERT INTO `week` (`id`, `week`) VALUES ('25',25);
INSERT INTO `week` (`id`, `week`) VALUES ('26',26);
INSERT INTO `week` (`id`, `week`) VALUES ('27',27);
INSERT INTO `week` (`id`, `week`) VALUES ('28',28);
INSERT INTO `week` (`id`, `week`) VALUES ('29',29);
INSERT INTO `week` (`id`, `week`) VALUES ('30',30);
INSERT INTO `week` (`id`, `week`) VALUES ('31',31);
INSERT INTO `week` (`id`, `week`) VALUES ('32',32);
INSERT INTO `week` (`id`, `week`) VALUES ('33',33);
INSERT INTO `week` (`id`, `week`) VALUES ('34',34);
INSERT INTO `week` (`id`, `week`) VALUES ('35',35);
INSERT INTO `week` (`id`, `week`) VALUES ('36',36);
INSERT INTO `week` (`id`, `week`) VALUES ('37',37);
INSERT INTO `week` (`id`, `week`) VALUES ('38',38);
INSERT INTO `week` (`id`, `week`) VALUES ('39',39);
INSERT INTO `week` (`id`, `week`) VALUES ('40',40);



--
-- Table structure for table gynecological_tests
--

CREATE TABLE IF NOT EXISTS  `gynecological_tests` (
    `id` int NOT NULL AUTO_INCREMENT,
    `week` int NOT NULL,
    `test_title` varchar(200) ,
    `test_details` varchar(700) ,
    `yourself_info` varchar(3000) ,
    `yourbaby_info` varchar(2500) ,
    `video` varchar(300) ,
    PRIMARY KEY (`id`)
)ENGINE=InnoDB DEFAULT CHARSET=utf8; 


--
-- Table structure for table bmi
--

CREATE TABLE IF NOT EXISTS  `bmi` (
    `id` int  NOT NULL AUTO_INCREMENT,
    `category` varchar(50) NOT NULL,
    PRIMARY KEY (`id`)
)ENGINE=InnoDB DEFAULT CHARSET=utf8; 


--
-- Initialize for table bmi
--
INSERT INTO `bmi` (`id`, `category`) VALUES (1,'UNDERWEIGHT');
INSERT INTO `bmi` (`id`, `category`) VALUES (2,'NORMALWEIGHT');
INSERT INTO `bmi` (`id`, `category`) VALUES (3,'OVERWEIGHT');
INSERT INTO `bmi` (`id`, `category`) VALUES (4,'OBESE');
INSERT INTO `bmi` (`id`, `category`) VALUES (5,'UNDERWEIGHT_TWINS');
INSERT INTO `bmi` (`id`, `category`) VALUES (6,'NORMALWEIGHT_TWINS');
INSERT INTO `bmi` (`id`, `category`) VALUES (7,'OVERWEIGHT_TWINS');
INSERT INTO `bmi` (`id`, `category`) VALUES (8,'OBESE_TWINS');


--
-- Table structure for table weight_limit
--

CREATE TABLE IF NOT EXISTS  `weight_limit` (
    `id` int  NOT NULL AUTO_INCREMENT,
    `week` int NOT NULL,
    `weight_max` double NOT NULL,
    `weight_min` double NOT NULL,
    `bmi` int NOT NULL,
    PRIMARY KEY (`id`),
	FOREIGN KEY (`bmi`) REFERENCES `bmi`(id),
    FOREIGN KEY (`week`) REFERENCES week(id)
)ENGINE=InnoDB DEFAULT CHARSET=utf8; 

--
-- Initialize for table weight_limit
--
INSERT INTO `weight_limit` (`id`, `week`,`weight_max`,`weight_min`,`bmi`) VALUES (1,1,0,0,2);
INSERT INTO `weight_limit` (`id`, `week`,`weight_max`,`weight_min`,`bmi`) VALUES (2,2,0.04,0.2,2);
INSERT INTO `weight_limit` (`id`, `week`,`weight_max`,`weight_min`,`bmi`) VALUES (3,3,0.08,0.3,2);
INSERT INTO `weight_limit` (`id`, `week`,`weight_max`,`weight_min`,`bmi`) VALUES (4,4,0.1,0.5,2);
INSERT INTO `weight_limit` (`id`, `week`,`weight_max`,`weight_min`,`bmi`) VALUES (5,5,0.2,0.7,2);
INSERT INTO `weight_limit` (`id`, `week`,`weight_max`,`weight_min`,`bmi`) VALUES (6,6,0.2,0.8,2);
INSERT INTO `weight_limit` (`id`, `week`,`weight_max`,`weight_min`,`bmi`) VALUES (7,7,0.2,1,2);
INSERT INTO `weight_limit` (`id`, `week`,`weight_max`,`weight_min`,`bmi`) VALUES (8,8,0.3,1.2,2);
INSERT INTO `weight_limit` (`id`, `week`,`weight_max`,`weight_min`,`bmi`) VALUES (9,9,0.3,1.3,2);
INSERT INTO `weight_limit` (`id`, `week`,`weight_max`,`weight_min`,`bmi`) VALUES (10,10,0.4,1.5,2);
INSERT INTO `weight_limit` (`id`, `week`,`weight_max`,`weight_min`,`bmi`) VALUES (11,11,0.4,1.7,2);
INSERT INTO `weight_limit` (`id`, `week`,`weight_max`,`weight_min`,`bmi`) VALUES (12,12,0.5,1.8,2);
INSERT INTO `weight_limit` (`id`, `week`,`weight_max`,`weight_min`,`bmi`) VALUES (13,13,0.5,2,2);
INSERT INTO `weight_limit` (`id`, `week`,`weight_max`,`weight_min`,`bmi`) VALUES (14,14,0.9,2.5,2);
INSERT INTO `weight_limit` (`id`, `week`,`weight_max`,`weight_min`,`bmi`) VALUES (15,15,1.3,3,2);
INSERT INTO `weight_limit` (`id`, `week`,`weight_max`,`weight_min`,`bmi`) VALUES (16,16,1.7,3.5,2);
INSERT INTO `weight_limit` (`id`, `week`,`weight_max`,`weight_min`,`bmi`) VALUES (17,17,2.1,4.1,2);
INSERT INTO `weight_limit` (`id`, `week`,`weight_max`,`weight_min`,`bmi`) VALUES (18,18,2.5,4.6,2);
INSERT INTO `weight_limit` (`id`, `week`,`weight_max`,`weight_min`,`bmi`) VALUES (19,19,2.9,5.1,2);
INSERT INTO `weight_limit` (`id`, `week`,`weight_max`,`weight_min`,`bmi`) VALUES (20,20,3.3,5.6,2);
INSERT INTO `weight_limit` (`id`, `week`,`weight_max`,`weight_min`,`bmi`) VALUES (21,21,3.7,6.1,2);
INSERT INTO `weight_limit` (`id`, `week`,`weight_max`,`weight_min`,`bmi`) VALUES (22,22,4.1,6.6,2);
INSERT INTO `weight_limit` (`id`, `week`,`weight_max`,`weight_min`,`bmi`) VALUES (23,23,4.5,7.1,2);
INSERT INTO `weight_limit` (`id`, `week`,`weight_max`,`weight_min`,`bmi`) VALUES (24,24,4.9,7.7,2);
INSERT INTO `weight_limit` (`id`, `week`,`weight_max`,`weight_min`,`bmi`) VALUES (25,25,5.3,8.2,2);
INSERT INTO `weight_limit` (`id`, `week`,`weight_max`,`weight_min`,`bmi`) VALUES (26,26,5.7,8.7,2);
INSERT INTO `weight_limit` (`id`, `week`,`weight_max`,`weight_min`,`bmi`) VALUES (27,27,6.1,9.2,2);
INSERT INTO `weight_limit` (`id`, `week`,`weight_max`,`weight_min`,`bmi`) VALUES (28,28,6.5,9.7,2);
INSERT INTO `weight_limit` (`id`, `week`,`weight_max`,`weight_min`,`bmi`) VALUES (29,29,6.9,10.2,2);
INSERT INTO `weight_limit` (`id`, `week`,`weight_max`,`weight_min`,`bmi`) VALUES (30,30,7.3,10.7,2);
INSERT INTO `weight_limit` (`id`, `week`,`weight_max`,`weight_min`,`bmi`) VALUES (31,31,7.7,11.2,2);
INSERT INTO `weight_limit` (`id`, `week`,`weight_max`,`weight_min`,`bmi`) VALUES (32,32,8.1,11.8,2);
INSERT INTO `weight_limit` (`id`, `week`,`weight_max`,`weight_min`,`bmi`) VALUES (33,33,8.5,12.3,2);
INSERT INTO `weight_limit` (`id`, `week`,`weight_max`,`weight_min`,`bmi`) VALUES (34,34,8.9,12.8,2);
INSERT INTO `weight_limit` (`id`, `week`,`weight_max`,`weight_min`,`bmi`) VALUES (35,35,9.3,13.3,2);
INSERT INTO `weight_limit` (`id`, `week`,`weight_max`,`weight_min`,`bmi`) VALUES (36,36,9.7,13.8,2);
INSERT INTO `weight_limit` (`id`, `week`,`weight_max`,`weight_min`,`bmi`) VALUES (37,37,10.1,14.3,2);
INSERT INTO `weight_limit` (`id`, `week`,`weight_max`,`weight_min`,`bmi`) VALUES (38,38,10.5,14.8,2);
INSERT INTO `weight_limit` (`id`, `week`,`weight_max`,`weight_min`,`bmi`) VALUES (39,39,10.9,15.4,2);
INSERT INTO `weight_limit` (`id`, `week`,`weight_max`,`weight_min`,`bmi`) VALUES (40,40,11.9,15.9,2);


INSERT INTO `weight_limit` (`id`, `week`,`weight_max`,`weight_min`,`bmi`) VALUES (41,1,0,0,1);
INSERT INTO `weight_limit` (`id`, `week`,`weight_max`,`weight_min`,`bmi`) VALUES (42,2,0.04,0.2,1);
INSERT INTO `weight_limit` (`id`, `week`,`weight_max`,`weight_min`,`bmi`) VALUES (43,3,0.1,0.3,1);
INSERT INTO `weight_limit` (`id`, `week`,`weight_max`,`weight_min`,`bmi`) VALUES (44,4,0.1,0.5,1);
INSERT INTO `weight_limit` (`id`, `week`,`weight_max`,`weight_min`,`bmi`) VALUES (45,5,0.2,0.7,1);
INSERT INTO `weight_limit` (`id`, `week`,`weight_max`,`weight_min`,`bmi`) VALUES (46,6,0.2,0.8,1);
INSERT INTO `weight_limit` (`id`, `week`,`weight_max`,`weight_min`,`bmi`) VALUES (47,7,0.3,1,1);
INSERT INTO `weight_limit` (`id`, `week`,`weight_max`,`weight_min`,`bmi`) VALUES (48,8,0.3,1.2,1);
INSERT INTO `weight_limit` (`id`, `week`,`weight_max`,`weight_min`,`bmi`) VALUES (49,9,0.3,1.3,1);
INSERT INTO `weight_limit` (`id`, `week`,`weight_max`,`weight_min`,`bmi`) VALUES (50,10,0.4,1.5,1);
INSERT INTO `weight_limit` (`id`, `week`,`weight_max`,`weight_min`,`bmi`) VALUES (51,11,0.4,1.7,1);
INSERT INTO `weight_limit` (`id`, `week`,`weight_max`,`weight_min`,`bmi`) VALUES (52,12,0.5,1.8,1);
INSERT INTO `weight_limit` (`id`, `week`,`weight_max`,`weight_min`,`bmi`) VALUES (53,13,0.5,2,1);
INSERT INTO `weight_limit` (`id`, `week`,`weight_max`,`weight_min`,`bmi`) VALUES (54,14,1,2.6,1);
INSERT INTO `weight_limit` (`id`, `week`,`weight_max`,`weight_min`,`bmi`) VALUES (55,15,1.4,3.2,1);
INSERT INTO `weight_limit` (`id`, `week`,`weight_max`,`weight_min`,`bmi`) VALUES (56,16,1.9,3.8,1);
INSERT INTO `weight_limit` (`id`, `week`,`weight_max`,`weight_min`,`bmi`) VALUES (57,17,2.3,4.4,1);
INSERT INTO `weight_limit` (`id`, `week`,`weight_max`,`weight_min`,`bmi`) VALUES (58,18,2.8,5,1);
INSERT INTO `weight_limit` (`id`, `week`,`weight_max`,`weight_min`,`bmi`) VALUES (59,19,3.2,5.6,1);
INSERT INTO `weight_limit` (`id`, `week`,`weight_max`,`weight_min`,`bmi`) VALUES (60,20,3.7,6.2,1);
INSERT INTO `weight_limit` (`id`, `week`,`weight_max`,`weight_min`,`bmi`) VALUES (61,21,4.1,6.8,1);
INSERT INTO `weight_limit` (`id`, `week`,`weight_max`,`weight_min`,`bmi`) VALUES (62,22,4.6,7.4,1);
INSERT INTO `weight_limit` (`id`, `week`,`weight_max`,`weight_min`,`bmi`) VALUES (63,23,5,8,1);
INSERT INTO `weight_limit` (`id`, `week`,`weight_max`,`weight_min`,`bmi`) VALUES (64,24,5.5,8.6,1);
INSERT INTO `weight_limit` (`id`, `week`,`weight_max`,`weight_min`,`bmi`) VALUES (65,25,5.9,9.2,1);
INSERT INTO `weight_limit` (`id`, `week`,`weight_max`,`weight_min`,`bmi`) VALUES (66,26,6.4,9.8,1);
INSERT INTO `weight_limit` (`id`, `week`,`weight_max`,`weight_min`,`bmi`) VALUES (67,27,6.8,10.4,1);
INSERT INTO `weight_limit` (`id`, `week`,`weight_max`,`weight_min`,`bmi`) VALUES (68,28,7.3,11,1);
INSERT INTO `weight_limit` (`id`, `week`,`weight_max`,`weight_min`,`bmi`) VALUES (69,29,7.7,11.6,1);
INSERT INTO `weight_limit` (`id`, `week`,`weight_max`,`weight_min`,`bmi`) VALUES (70,30,8.2,12.2,1);
INSERT INTO `weight_limit` (`id`, `week`,`weight_max`,`weight_min`,`bmi`) VALUES (71,31,8.6,12.7,1);
INSERT INTO `weight_limit` (`id`, `week`,`weight_max`,`weight_min`,`bmi`) VALUES (72,32,9.1,13.4,1);
INSERT INTO `weight_limit` (`id`, `week`,`weight_max`,`weight_min`,`bmi`) VALUES (73,33,9.5,14,1);
INSERT INTO `weight_limit` (`id`, `week`,`weight_max`,`weight_min`,`bmi`) VALUES (74,34,10,14.6,1);
INSERT INTO `weight_limit` (`id`, `week`,`weight_max`,`weight_min`,`bmi`) VALUES (75,35,10.4,15.1,1);
INSERT INTO `weight_limit` (`id`, `week`,`weight_max`,`weight_min`,`bmi`) VALUES (76,36,10.9,15.7,1);
INSERT INTO `weight_limit` (`id`, `week`,`weight_max`,`weight_min`,`bmi`) VALUES (77,37,11.3,16.3,1);
INSERT INTO `weight_limit` (`id`, `week`,`weight_max`,`weight_min`,`bmi`) VALUES (78,38,11.8,17,1);
INSERT INTO `weight_limit` (`id`, `week`,`weight_max`,`weight_min`,`bmi`) VALUES (79,39,12.2,17.6,1);
INSERT INTO `weight_limit` (`id`, `week`,`weight_max`,`weight_min`,`bmi`) VALUES (80,40,12.7,18.1,1);


INSERT INTO `weight_limit` (`id`, `week`,`weight_max`,`weight_min`,`bmi`) VALUES (81,1,0,0,3);
INSERT INTO `weight_limit` (`id`, `week`,`weight_max`,`weight_min`,`bmi`) VALUES (82,2,0.04,0.2,3);
INSERT INTO `weight_limit` (`id`, `week`,`weight_max`,`weight_min`,`bmi`) VALUES (83,3,0.1,0.3,3);
INSERT INTO `weight_limit` (`id`, `week`,`weight_max`,`weight_min`,`bmi`) VALUES (84,4,0.1,0.5,3);
INSERT INTO `weight_limit` (`id`, `week`,`weight_max`,`weight_min`,`bmi`) VALUES (85,5,0.2,0.7,3);
INSERT INTO `weight_limit` (`id`, `week`,`weight_max`,`weight_min`,`bmi`) VALUES (86,6,0.2,0.8,3);
INSERT INTO `weight_limit` (`id`, `week`,`weight_max`,`weight_min`,`bmi`) VALUES (87,7,0.3,1,3);
INSERT INTO `weight_limit` (`id`, `week`,`weight_max`,`weight_min`,`bmi`) VALUES (88,8,0.3,1.2,3);
INSERT INTO `weight_limit` (`id`, `week`,`weight_max`,`weight_min`,`bmi`) VALUES (89,9,0.3,1.3,3);
INSERT INTO `weight_limit` (`id`, `week`,`weight_max`,`weight_min`,`bmi`) VALUES (90,10,0.4,1.5,3);
INSERT INTO `weight_limit` (`id`, `week`,`weight_max`,`weight_min`,`bmi`) VALUES (91,11,0.4,1.7,3);
INSERT INTO `weight_limit` (`id`, `week`,`weight_max`,`weight_min`,`bmi`) VALUES (92,12,0.5,1.8,3);
INSERT INTO `weight_limit` (`id`, `week`,`weight_max`,`weight_min`,`bmi`) VALUES (93,13,0.5,2,3);
INSERT INTO `weight_limit` (`id`, `week`,`weight_max`,`weight_min`,`bmi`) VALUES (94,14,0.7,2.4,3);
INSERT INTO `weight_limit` (`id`, `week`,`weight_max`,`weight_min`,`bmi`) VALUES (95,15,1,2.7,3);
INSERT INTO `weight_limit` (`id`, `week`,`weight_max`,`weight_min`,`bmi`) VALUES (96,16,1.2,3,3);
INSERT INTO `weight_limit` (`id`, `week`,`weight_max`,`weight_min`,`bmi`) VALUES (97,17,1.5,3.4,3);
INSERT INTO `weight_limit` (`id`, `week`,`weight_max`,`weight_min`,`bmi`) VALUES (98,18,1.7,3.7,3);
INSERT INTO `weight_limit` (`id`, `week`,`weight_max`,`weight_min`,`bmi`) VALUES (99,19,1.9,4.1,3);
INSERT INTO `weight_limit` (`id`, `week`,`weight_max`,`weight_min`,`bmi`) VALUES (100,20,2.1,4.4,3);
INSERT INTO `weight_limit` (`id`, `week`,`weight_max`,`weight_min`,`bmi`) VALUES (101,21,2.4,4.8,3);
INSERT INTO `weight_limit` (`id`, `week`,`weight_max`,`weight_min`,`bmi`) VALUES (102,22,2.6,5.1,3);
INSERT INTO `weight_limit` (`id`, `week`,`weight_max`,`weight_min`,`bmi`) VALUES (103,23,2.8,5.4,3);
INSERT INTO `weight_limit` (`id`, `week`,`weight_max`,`weight_min`,`bmi`) VALUES (104,24,3.1,5.8,3);
INSERT INTO `weight_limit` (`id`, `week`,`weight_max`,`weight_min`,`bmi`) VALUES (105,25,3.3,6.2,3);
INSERT INTO `weight_limit` (`id`, `week`,`weight_max`,`weight_min`,`bmi`) VALUES (106,26,3.5,6.5,3);
INSERT INTO `weight_limit` (`id`, `week`,`weight_max`,`weight_min`,`bmi`) VALUES (107,27,3.8,6.8,3);
INSERT INTO `weight_limit` (`id`, `week`,`weight_max`,`weight_min`,`bmi`) VALUES (108,28,4,7.2,3);
INSERT INTO `weight_limit` (`id`, `week`,`weight_max`,`weight_min`,`bmi`) VALUES (109,29,4.2,7.5,3);
INSERT INTO `weight_limit` (`id`, `week`,`weight_max`,`weight_min`,`bmi`) VALUES (110,30,4.5,7.9,3);
INSERT INTO `weight_limit` (`id`, `week`,`weight_max`,`weight_min`,`bmi`) VALUES (111,31,4.7,8.2,3);
INSERT INTO `weight_limit` (`id`, `week`,`weight_max`,`weight_min`,`bmi`) VALUES (112,32,4.9,8.6,3);
INSERT INTO `weight_limit` (`id`, `week`,`weight_max`,`weight_min`,`bmi`) VALUES (113,33,5.2,8.9,3);
INSERT INTO `weight_limit` (`id`, `week`,`weight_max`,`weight_min`,`bmi`) VALUES (114,34,5.4,9.3,3);
INSERT INTO `weight_limit` (`id`, `week`,`weight_max`,`weight_min`,`bmi`) VALUES (115,35,5.6,9.6,3);
INSERT INTO `weight_limit` (`id`, `week`,`weight_max`,`weight_min`,`bmi`) VALUES (116,36,5.9,9.9,3);
INSERT INTO `weight_limit` (`id`, `week`,`weight_max`,`weight_min`,`bmi`) VALUES (117,37,6.1,10.3,3);
INSERT INTO `weight_limit` (`id`, `week`,`weight_max`,`weight_min`,`bmi`) VALUES (118,38,6.4,10.7,3);
INSERT INTO `weight_limit` (`id`, `week`,`weight_max`,`weight_min`,`bmi`) VALUES (119,39,6.6,11,3);
INSERT INTO `weight_limit` (`id`, `week`,`weight_max`,`weight_min`,`bmi`) VALUES (120,40,6.8,11.3,3);


INSERT INTO `weight_limit` (`id`, `week`,`weight_max`,`weight_min`,`bmi`) VALUES (121,1,0,0,4);
INSERT INTO `weight_limit` (`id`, `week`,`weight_max`,`weight_min`,`bmi`) VALUES (122,2,0,0.2,4);
INSERT INTO `weight_limit` (`id`, `week`,`weight_max`,`weight_min`,`bmi`) VALUES (123,3,0.1,0.3,4);
INSERT INTO `weight_limit` (`id`, `week`,`weight_max`,`weight_min`,`bmi`) VALUES (124,4,0.1,0.5,4);
INSERT INTO `weight_limit` (`id`, `week`,`weight_max`,`weight_min`,`bmi`) VALUES (125,5,0.2,0.7,4);
INSERT INTO `weight_limit` (`id`, `week`,`weight_max`,`weight_min`,`bmi`) VALUES (126,6,0.2,0.8,4);
INSERT INTO `weight_limit` (`id`, `week`,`weight_max`,`weight_min`,`bmi`) VALUES (127,7,0.3,1,4);
INSERT INTO `weight_limit` (`id`, `week`,`weight_max`,`weight_min`,`bmi`) VALUES (128,8,0.3,1.2,4);
INSERT INTO `weight_limit` (`id`, `week`,`weight_max`,`weight_min`,`bmi`) VALUES (129,9,0.3,1.3,4);
INSERT INTO `weight_limit` (`id`, `week`,`weight_max`,`weight_min`,`bmi`) VALUES (130,10,0.4,1.5,4);
INSERT INTO `weight_limit` (`id`, `week`,`weight_max`,`weight_min`,`bmi`) VALUES (131,11,0.4,1.7,4);
INSERT INTO `weight_limit` (`id`, `week`,`weight_max`,`weight_min`,`bmi`) VALUES (132,12,0.5,1.8,4);
INSERT INTO `weight_limit` (`id`, `week`,`weight_max`,`weight_min`,`bmi`) VALUES (133,13,0.5,2,4);
INSERT INTO `weight_limit` (`id`, `week`,`weight_max`,`weight_min`,`bmi`) VALUES (134,14,0.7,2.3,4);
INSERT INTO `weight_limit` (`id`, `week`,`weight_max`,`weight_min`,`bmi`) VALUES (135,15,0.8,2.5,4);
INSERT INTO `weight_limit` (`id`, `week`,`weight_max`,`weight_min`,`bmi`) VALUES (136,16,1,2.8,4);
INSERT INTO `weight_limit` (`id`, `week`,`weight_max`,`weight_min`,`bmi`) VALUES (137,17,1.2,3,4);
INSERT INTO `weight_limit` (`id`, `week`,`weight_max`,`weight_min`,`bmi`) VALUES (138,18,1.3,3.3,4);
INSERT INTO `weight_limit` (`id`, `week`,`weight_max`,`weight_min`,`bmi`) VALUES (139,19,1.5,3.6,4);
INSERT INTO `weight_limit` (`id`, `week`,`weight_max`,`weight_min`,`bmi`) VALUES (140,20,1.7,3.8,4);
INSERT INTO `weight_limit` (`id`, `week`,`weight_max`,`weight_min`,`bmi`) VALUES (141,21,1.8,4.1,4);
INSERT INTO `weight_limit` (`id`, `week`,`weight_max`,`weight_min`,`bmi`) VALUES (142,22,2,4.4,4);
INSERT INTO `weight_limit` (`id`, `week`,`weight_max`,`weight_min`,`bmi`) VALUES (143,23,2.2,4.6,4);
INSERT INTO `weight_limit` (`id`, `week`,`weight_max`,`weight_min`,`bmi`) VALUES (144,24,2.3,4.9,4);
INSERT INTO `weight_limit` (`id`, `week`,`weight_max`,`weight_min`,`bmi`) VALUES (145,25,2.5,5.1,4);
INSERT INTO `weight_limit` (`id`, `week`,`weight_max`,`weight_min`,`bmi`) VALUES (146,26,2.7,5.4,4);
INSERT INTO `weight_limit` (`id`, `week`,`weight_max`,`weight_min`,`bmi`) VALUES (147,27,2.8,5.7,4);
INSERT INTO `weight_limit` (`id`, `week`,`weight_max`,`weight_min`,`bmi`) VALUES (148,28,3,5.9,4);
INSERT INTO `weight_limit` (`id`, `week`,`weight_max`,`weight_min`,`bmi`) VALUES (149,29,3.2,6.2,4);
INSERT INTO `weight_limit` (`id`, `week`,`weight_max`,`weight_min`,`bmi`) VALUES (150,30,3.3,6.4,4);
INSERT INTO `weight_limit` (`id`, `week`,`weight_max`,`weight_min`,`bmi`) VALUES (151,31,3.5,6.7,4);
INSERT INTO `weight_limit` (`id`, `week`,`weight_max`,`weight_min`,`bmi`) VALUES (152,32,3.7,7,4);
INSERT INTO `weight_limit` (`id`, `week`,`weight_max`,`weight_min`,`bmi`) VALUES (153,33,3.8,7.3,4);
INSERT INTO `weight_limit` (`id`, `week`,`weight_max`,`weight_min`,`bmi`) VALUES (154,34,4,7.5,4);
INSERT INTO `weight_limit` (`id`, `week`,`weight_max`,`weight_min`,`bmi`) VALUES (155,35,4.2,7.8,4);
INSERT INTO `weight_limit` (`id`, `week`,`weight_max`,`weight_min`,`bmi`) VALUES (156,36,4.3,8,4);
INSERT INTO `weight_limit` (`id`, `week`,`weight_max`,`weight_min`,`bmi`) VALUES (157,37,4.5,8.3,4);
INSERT INTO `weight_limit` (`id`, `week`,`weight_max`,`weight_min`,`bmi`) VALUES (158,38,4.7,8.5,4);
INSERT INTO `weight_limit` (`id`, `week`,`weight_max`,`weight_min`,`bmi`) VALUES (159,39,4.8,8.8,4);
INSERT INTO `weight_limit` (`id`, `week`,`weight_max`,`weight_min`,`bmi`) VALUES (160,40,5,9.1,4);


INSERT INTO `weight_limit` (`id`, `week`,`weight_max`,`weight_min`,`bmi`) VALUES (161,1,0,0,5);
INSERT INTO `weight_limit` (`id`, `week`,`weight_max`,`weight_min`,`bmi`) VALUES (162,2,0.04,0.2,5);
INSERT INTO `weight_limit` (`id`, `week`,`weight_max`,`weight_min`,`bmi`) VALUES (163,3,0.1,0.3,5);
INSERT INTO `weight_limit` (`id`, `week`,`weight_max`,`weight_min`,`bmi`) VALUES (164,4,0.1,0.5,5);
INSERT INTO `weight_limit` (`id`, `week`,`weight_max`,`weight_min`,`bmi`) VALUES (165,5,0.2,0.7,5);
INSERT INTO `weight_limit` (`id`, `week`,`weight_max`,`weight_min`,`bmi`) VALUES (166,6,0.2,0.8,5);
INSERT INTO `weight_limit` (`id`, `week`,`weight_max`,`weight_min`,`bmi`) VALUES (167,7,0.3,1,5);
INSERT INTO `weight_limit` (`id`, `week`,`weight_max`,`weight_min`,`bmi`) VALUES (168,8,0.3,1.2,5);
INSERT INTO `weight_limit` (`id`, `week`,`weight_max`,`weight_min`,`bmi`) VALUES (169,9,0.3,1.3,5);
INSERT INTO `weight_limit` (`id`, `week`,`weight_max`,`weight_min`,`bmi`) VALUES (170,10,0.4,1.5,5);
INSERT INTO `weight_limit` (`id`, `week`,`weight_max`,`weight_min`,`bmi`) VALUES (171,11,0.4,1.7,5);
INSERT INTO `weight_limit` (`id`, `week`,`weight_max`,`weight_min`,`bmi`) VALUES (172,12,0.5,1.8,5);
INSERT INTO `weight_limit` (`id`, `week`,`weight_max`,`weight_min`,`bmi`) VALUES (173,13,0.5,2,5);
INSERT INTO `weight_limit` (`id`, `week`,`weight_max`,`weight_min`,`bmi`) VALUES (174,14,1.1,2.8,5);
INSERT INTO `weight_limit` (`id`, `week`,`weight_max`,`weight_min`,`bmi`) VALUES (175,15,1.7,3.7,5);
INSERT INTO `weight_limit` (`id`, `week`,`weight_max`,`weight_min`,`bmi`) VALUES (176,16,2.3,4.5,5);
INSERT INTO `weight_limit` (`id`, `week`,`weight_max`,`weight_min`,`bmi`) VALUES (177,17,2.9,5.3,5);
INSERT INTO `weight_limit` (`id`, `week`,`weight_max`,`weight_min`,`bmi`) VALUES (178,18,3.5,6.2,5);
INSERT INTO `weight_limit` (`id`, `week`,`weight_max`,`weight_min`,`bmi`) VALUES (179,19,4.1,7,5);
INSERT INTO `weight_limit` (`id`, `week`,`weight_max`,`weight_min`,`bmi`) VALUES (180,20,4.7,7.8,5);
INSERT INTO `weight_limit` (`id`, `week`,`weight_max`,`weight_min`,`bmi`) VALUES (181,21,5.3,8.7,5);
INSERT INTO `weight_limit` (`id`, `week`,`weight_max`,`weight_min`,`bmi`) VALUES (182,22,5.9,9.5,5);
INSERT INTO `weight_limit` (`id`, `week`,`weight_max`,`weight_min`,`bmi`) VALUES (183,23,6.5,10.3,5);
INSERT INTO `weight_limit` (`id`, `week`,`weight_max`,`weight_min`,`bmi`) VALUES (184,24,7.1,11.2,5);
INSERT INTO `weight_limit` (`id`, `week`,`weight_max`,`weight_min`,`bmi`) VALUES (185,25,7.8,12,5);
INSERT INTO `weight_limit` (`id`, `week`,`weight_max`,`weight_min`,`bmi`) VALUES (186,26,8.3,12.8,5);
INSERT INTO `weight_limit` (`id`, `week`,`weight_max`,`weight_min`,`bmi`) VALUES (187,27,8.9,13.7,5);
INSERT INTO `weight_limit` (`id`, `week`,`weight_max`,`weight_min`,`bmi`) VALUES (188,28,9.5,14.5,5);
INSERT INTO `weight_limit` (`id`, `week`,`weight_max`,`weight_min`,`bmi`) VALUES (189,29,10.2,15.3,5);
INSERT INTO `weight_limit` (`id`, `week`,`weight_max`,`weight_min`,`bmi`) VALUES (190,30,10.8,16.1,5);
INSERT INTO `weight_limit` (`id`, `week`,`weight_max`,`weight_min`,`bmi`) VALUES (191,31,11.3,17,5);
INSERT INTO `weight_limit` (`id`, `week`,`weight_max`,`weight_min`,`bmi`) VALUES (192,32,12,17.8,5);
INSERT INTO `weight_limit` (`id`, `week`,`weight_max`,`weight_min`,`bmi`) VALUES (193,33,12.6,18.6,5);
INSERT INTO `weight_limit` (`id`, `week`,`weight_max`,`weight_min`,`bmi`) VALUES (194,34,13.2,19.5,5);
INSERT INTO `weight_limit` (`id`, `week`,`weight_max`,`weight_min`,`bmi`) VALUES (195,35,13.8,20.3,5);
INSERT INTO `weight_limit` (`id`, `week`,`weight_max`,`weight_min`,`bmi`) VALUES (196,36,14.4,21.2,5);
INSERT INTO `weight_limit` (`id`, `week`,`weight_max`,`weight_min`,`bmi`) VALUES (197,37,15,22,5);
INSERT INTO `weight_limit` (`id`, `week`,`weight_max`,`weight_min`,`bmi`) VALUES (198,38,15.6,22.8,5);
INSERT INTO `weight_limit` (`id`, `week`,`weight_max`,`weight_min`,`bmi`) VALUES (199,39,16.2,23.7,5);
INSERT INTO `weight_limit` (`id`, `week`,`weight_max`,`weight_min`,`bmi`) VALUES (200,40,16.8,24.5,5);


INSERT INTO `weight_limit` (`id`, `week`,`weight_max`,`weight_min`,`bmi`) VALUES (201,1,0,0,6);
INSERT INTO `weight_limit` (`id`, `week`,`weight_max`,`weight_min`,`bmi`) VALUES (202,2,0.04,0.2,6);
INSERT INTO `weight_limit` (`id`, `week`,`weight_max`,`weight_min`,`bmi`) VALUES (203,3,0.1,0.3,6);
INSERT INTO `weight_limit` (`id`, `week`,`weight_max`,`weight_min`,`bmi`) VALUES (204,4,0.1,0.5,6);
INSERT INTO `weight_limit` (`id`, `week`,`weight_max`,`weight_min`,`bmi`) VALUES (205,5,0.2,0.7,6);
INSERT INTO `weight_limit` (`id`, `week`,`weight_max`,`weight_min`,`bmi`) VALUES (206,6,0.2,0.8,6);
INSERT INTO `weight_limit` (`id`, `week`,`weight_max`,`weight_min`,`bmi`) VALUES (207,7,0.3,1,6);
INSERT INTO `weight_limit` (`id`, `week`,`weight_max`,`weight_min`,`bmi`) VALUES (208,8,0.3,1.2,6);
INSERT INTO `weight_limit` (`id`, `week`,`weight_max`,`weight_min`,`bmi`) VALUES (209,9,0.3,1.3,6);
INSERT INTO `weight_limit` (`id`, `week`,`weight_max`,`weight_min`,`bmi`) VALUES (210,10,0.4,1.5,6);
INSERT INTO `weight_limit` (`id`, `week`,`weight_max`,`weight_min`,`bmi`) VALUES (211,11,0.4,1.7,6);
INSERT INTO `weight_limit` (`id`, `week`,`weight_max`,`weight_min`,`bmi`) VALUES (212,12,0.5,1.8,6);
INSERT INTO `weight_limit` (`id`, `week`,`weight_max`,`weight_min`,`bmi`) VALUES (213,13,0.5,2,6);
INSERT INTO `weight_limit` (`id`, `week`,`weight_max`,`weight_min`,`bmi`) VALUES (214,14,1,2.8,6);
INSERT INTO `weight_limit` (`id`, `week`,`weight_max`,`weight_min`,`bmi`) VALUES (215,15,1.5,3.5,6);
INSERT INTO `weight_limit` (`id`, `week`,`weight_max`,`weight_min`,`bmi`) VALUES (216,16,2,4.3,6);
INSERT INTO `weight_limit` (`id`, `week`,`weight_max`,`weight_min`,`bmi`) VALUES (217,17,2.5,5.1,6);
INSERT INTO `weight_limit` (`id`, `week`,`weight_max`,`weight_min`,`bmi`) VALUES (218,18,3,5.8,6);
INSERT INTO `weight_limit` (`id`, `week`,`weight_max`,`weight_min`,`bmi`) VALUES (219,19,3.5,6.6,6);
INSERT INTO `weight_limit` (`id`, `week`,`weight_max`,`weight_min`,`bmi`) VALUES (220,20,4,7.3,6);
INSERT INTO `weight_limit` (`id`, `week`,`weight_max`,`weight_min`,`bmi`) VALUES (221,21,4.5,8.1,6);
INSERT INTO `weight_limit` (`id`, `week`,`weight_max`,`weight_min`,`bmi`) VALUES (222,22,5,8.9,6);
INSERT INTO `weight_limit` (`id`, `week`,`weight_max`,`weight_min`,`bmi`) VALUES (223,23,5.5,9.7,6);
INSERT INTO `weight_limit` (`id`, `week`,`weight_max`,`weight_min`,`bmi`) VALUES (224,24,6,10.4,6);
INSERT INTO `weight_limit` (`id`, `week`,`weight_max`,`weight_min`,`bmi`) VALUES (225,25,6.5,11.2,6);
INSERT INTO `weight_limit` (`id`, `week`,`weight_max`,`weight_min`,`bmi`) VALUES (226,26,7,12,6);
INSERT INTO `weight_limit` (`id`, `week`,`weight_max`,`weight_min`,`bmi`) VALUES (227,27,7.5,12.7,6);
INSERT INTO `weight_limit` (`id`, `week`,`weight_max`,`weight_min`,`bmi`) VALUES (228,28,8,13.5,6);
INSERT INTO `weight_limit` (`id`, `week`,`weight_max`,`weight_min`,`bmi`) VALUES (229,29,8.5,14.2,6);
INSERT INTO `weight_limit` (`id`, `week`,`weight_max`,`weight_min`,`bmi`) VALUES (230,30,9,15,6);
INSERT INTO `weight_limit` (`id`, `week`,`weight_max`,`weight_min`,`bmi`) VALUES (231,31,9.5,15.8,6);
INSERT INTO `weight_limit` (`id`, `week`,`weight_max`,`weight_min`,`bmi`) VALUES (232,32,10,16.6,6);
INSERT INTO `weight_limit` (`id`, `week`,`weight_max`,`weight_min`,`bmi`) VALUES (233,33,10.5,17.3,6);
INSERT INTO `weight_limit` (`id`, `week`,`weight_max`,`weight_min`,`bmi`) VALUES (234,34,11.1,18.1,6);
INSERT INTO `weight_limit` (`id`, `week`,`weight_max`,`weight_min`,`bmi`) VALUES (235,35,11.6,18.9,6);
INSERT INTO `weight_limit` (`id`, `week`,`weight_max`,`weight_min`,`bmi`) VALUES (236,36,12.1,19.6,6);
INSERT INTO `weight_limit` (`id`, `week`,`weight_max`,`weight_min`,`bmi`) VALUES (237,37,12.6,20.4,6);
INSERT INTO `weight_limit` (`id`, `week`,`weight_max`,`weight_min`,`bmi`) VALUES (238,38,13.1,21.1,6);
INSERT INTO `weight_limit` (`id`, `week`,`weight_max`,`weight_min`,`bmi`) VALUES (239,39,13.6,21.9,6);
INSERT INTO `weight_limit` (`id`, `week`,`weight_max`,`weight_min`,`bmi`) VALUES (240,40,14.1,22.7,6);


INSERT INTO `weight_limit` (`id`, `week`,`weight_max`,`weight_min`,`bmi`) VALUES (241,1,0,0,7);
INSERT INTO `weight_limit` (`id`, `week`,`weight_max`,`weight_min`,`bmi`) VALUES (242,2,0.04,0.2,7);
INSERT INTO `weight_limit` (`id`, `week`,`weight_max`,`weight_min`,`bmi`) VALUES (243,3,0.1,0.3,7);
INSERT INTO `weight_limit` (`id`, `week`,`weight_max`,`weight_min`,`bmi`) VALUES (244,4,0.1,0.5,7);
INSERT INTO `weight_limit` (`id`, `week`,`weight_max`,`weight_min`,`bmi`) VALUES (245,5,0.2,0.7,7);
INSERT INTO `weight_limit` (`id`, `week`,`weight_max`,`weight_min`,`bmi`) VALUES (246,6,0.2,0.8,7);
INSERT INTO `weight_limit` (`id`, `week`,`weight_max`,`weight_min`,`bmi`) VALUES (247,7,0.3,1,7);
INSERT INTO `weight_limit` (`id`, `week`,`weight_max`,`weight_min`,`bmi`) VALUES (248,8,0.3,1.2,7);
INSERT INTO `weight_limit` (`id`, `week`,`weight_max`,`weight_min`,`bmi`) VALUES (249,9,0.3,1.3,7);
INSERT INTO `weight_limit` (`id`, `week`,`weight_max`,`weight_min`,`bmi`) VALUES (250,10,0.4,1.5,7);
INSERT INTO `weight_limit` (`id`, `week`,`weight_max`,`weight_min`,`bmi`) VALUES (251,11,0.4,1.7,7);
INSERT INTO `weight_limit` (`id`, `week`,`weight_max`,`weight_min`,`bmi`) VALUES (252,12,0.5,1.8,7);
INSERT INTO `weight_limit` (`id`, `week`,`weight_max`,`weight_min`,`bmi`) VALUES (253,13,0.5,2,7);
INSERT INTO `weight_limit` (`id`, `week`,`weight_max`,`weight_min`,`bmi`) VALUES (254,14,0.9,2.6,7);
INSERT INTO `weight_limit` (`id`, `week`,`weight_max`,`weight_min`,`bmi`) VALUES (255,15,1.3,3.3,7);
INSERT INTO `weight_limit` (`id`, `week`,`weight_max`,`weight_min`,`bmi`) VALUES (256,16,1.7,3.9,7);
INSERT INTO `weight_limit` (`id`, `week`,`weight_max`,`weight_min`,`bmi`) VALUES (257,17,2.1,4.5,7);
INSERT INTO `weight_limit` (`id`, `week`,`weight_max`,`weight_min`,`bmi`) VALUES (258,18,2.5,5.2,7);
INSERT INTO `weight_limit` (`id`, `week`,`weight_max`,`weight_min`,`bmi`) VALUES (259,19,2.9,5.8,7);
INSERT INTO `weight_limit` (`id`, `week`,`weight_max`,`weight_min`,`bmi`) VALUES (260,20,3.3,6.4,7);
INSERT INTO `weight_limit` (`id`, `week`,`weight_max`,`weight_min`,`bmi`) VALUES (261,21,3.7,7,7);
INSERT INTO `weight_limit` (`id`, `week`,`weight_max`,`weight_min`,`bmi`) VALUES (262,22,4.1,7.7,7);
INSERT INTO `weight_limit` (`id`, `week`,`weight_max`,`weight_min`,`bmi`) VALUES (263,23,4.5,8.3,7);
INSERT INTO `weight_limit` (`id`, `week`,`weight_max`,`weight_min`,`bmi`) VALUES (264,24,4.9,8.9,7);
INSERT INTO `weight_limit` (`id`, `week`,`weight_max`,`weight_min`,`bmi`) VALUES (265,25,5.3,9.6,7);
INSERT INTO `weight_limit` (`id`, `week`,`weight_max`,`weight_min`,`bmi`) VALUES (266,26,5.7,10.2,7);
INSERT INTO `weight_limit` (`id`, `week`,`weight_max`,`weight_min`,`bmi`) VALUES (267,27,6.1,10.8,7);
INSERT INTO `weight_limit` (`id`, `week`,`weight_max`,`weight_min`,`bmi`) VALUES (268,28,6.5,11.5,7);
INSERT INTO `weight_limit` (`id`, `week`,`weight_max`,`weight_min`,`bmi`) VALUES (269,29,6.9,12.1,7);
INSERT INTO `weight_limit` (`id`, `week`,`weight_max`,`weight_min`,`bmi`) VALUES (270,30,7.3,12.7,7);
INSERT INTO `weight_limit` (`id`, `week`,`weight_max`,`weight_min`,`bmi`) VALUES (271,31,7.7,13.4,7);
INSERT INTO `weight_limit` (`id`, `week`,`weight_max`,`weight_min`,`bmi`) VALUES (272,32,8.1,14,7);
INSERT INTO `weight_limit` (`id`, `week`,`weight_max`,`weight_min`,`bmi`) VALUES (273,33,8.5,14.7,7);
INSERT INTO `weight_limit` (`id`, `week`,`weight_max`,`weight_min`,`bmi`) VALUES (274,34,8.9,15.2,7);
INSERT INTO `weight_limit` (`id`, `week`,`weight_max`,`weight_min`,`bmi`) VALUES (275,35,9.3,15.9,7);
INSERT INTO `weight_limit` (`id`, `week`,`weight_max`,`weight_min`,`bmi`) VALUES (276,36,9.8,16.5,7);
INSERT INTO `weight_limit` (`id`, `week`,`weight_max`,`weight_min`,`bmi`) VALUES (277,37,10.1,17.1,7);
INSERT INTO `weight_limit` (`id`, `week`,`weight_max`,`weight_min`,`bmi`) VALUES (278,38,10.5,17.8,7);
INSERT INTO `weight_limit` (`id`, `week`,`weight_max`,`weight_min`,`bmi`) VALUES (279,39,10.9,18.4,7);
INSERT INTO `weight_limit` (`id`, `week`,`weight_max`,`weight_min`,`bmi`) VALUES (280,40,11.3,19.1,7);



INSERT INTO `weight_limit` (`id`, `week`,`weight_max`,`weight_min`,`bmi`) VALUES (281,1,0,0,8);
INSERT INTO `weight_limit` (`id`, `week`,`weight_max`,`weight_min`,`bmi`) VALUES (282,2,0,0.2,8);
INSERT INTO `weight_limit` (`id`, `week`,`weight_max`,`weight_min`,`bmi`) VALUES (283,3,0.1,0.3,8);
INSERT INTO `weight_limit` (`id`, `week`,`weight_max`,`weight_min`,`bmi`) VALUES (284,4,0.1,0.5,8);
INSERT INTO `weight_limit` (`id`, `week`,`weight_max`,`weight_min`,`bmi`) VALUES (285,5,0.2,0.7,8);
INSERT INTO `weight_limit` (`id`, `week`,`weight_max`,`weight_min`,`bmi`) VALUES (286,6,0.2,0.8,8);
INSERT INTO `weight_limit` (`id`, `week`,`weight_max`,`weight_min`,`bmi`) VALUES (287,7,0.3,1,8);
INSERT INTO `weight_limit` (`id`, `week`,`weight_max`,`weight_min`,`bmi`) VALUES (288,8,0.3,1.2,8);
INSERT INTO `weight_limit` (`id`, `week`,`weight_max`,`weight_min`,`bmi`) VALUES (289,9,0.3,1.3,8);
INSERT INTO `weight_limit` (`id`, `week`,`weight_max`,`weight_min`,`bmi`) VALUES (290,10,0.4,1.5,8);
INSERT INTO `weight_limit` (`id`, `week`,`weight_max`,`weight_min`,`bmi`) VALUES (291,11,0.4,1.7,8);
INSERT INTO `weight_limit` (`id`, `week`,`weight_max`,`weight_min`,`bmi`) VALUES (292,12,0.5,1.8,8);
INSERT INTO `weight_limit` (`id`, `week`,`weight_max`,`weight_min`,`bmi`) VALUES (293,13,0.5,2,8);
INSERT INTO `weight_limit` (`id`, `week`,`weight_max`,`weight_min`,`bmi`) VALUES (294,14,0.9,2.6,8);
INSERT INTO `weight_limit` (`id`, `week`,`weight_max`,`weight_min`,`bmi`) VALUES (295,15,1.3,3.3,8);
INSERT INTO `weight_limit` (`id`, `week`,`weight_max`,`weight_min`,`bmi`) VALUES (296,16,1.7,3.9,8);
INSERT INTO `weight_limit` (`id`, `week`,`weight_max`,`weight_min`,`bmi`) VALUES (297,17,2.1,4.5,8);
INSERT INTO `weight_limit` (`id`, `week`,`weight_max`,`weight_min`,`bmi`) VALUES (298,18,2.5,5.2,8);
INSERT INTO `weight_limit` (`id`, `week`,`weight_max`,`weight_min`,`bmi`) VALUES (299,19,2.9,5.8,8);
INSERT INTO `weight_limit` (`id`, `week`,`weight_max`,`weight_min`,`bmi`) VALUES (300,20,3.3,6.4,8);
INSERT INTO `weight_limit` (`id`, `week`,`weight_max`,`weight_min`,`bmi`) VALUES (301,21,3.7,7,8);
INSERT INTO `weight_limit` (`id`, `week`,`weight_max`,`weight_min`,`bmi`) VALUES (302,22,4.1,7.7,8);
INSERT INTO `weight_limit` (`id`, `week`,`weight_max`,`weight_min`,`bmi`) VALUES (303,23,4.5,8.3,8);
INSERT INTO `weight_limit` (`id`, `week`,`weight_max`,`weight_min`,`bmi`) VALUES (304,24,4.9,8.9,8);
INSERT INTO `weight_limit` (`id`, `week`,`weight_max`,`weight_min`,`bmi`) VALUES (305,25,5.3,9.6,8);
INSERT INTO `weight_limit` (`id`, `week`,`weight_max`,`weight_min`,`bmi`) VALUES (306,26,5.7,10.2,8);
INSERT INTO `weight_limit` (`id`, `week`,`weight_max`,`weight_min`,`bmi`) VALUES (307,27,6.1,10.8,8);
INSERT INTO `weight_limit` (`id`, `week`,`weight_max`,`weight_min`,`bmi`) VALUES (308,28,6.5,11.5,8);
INSERT INTO `weight_limit` (`id`, `week`,`weight_max`,`weight_min`,`bmi`) VALUES (309,29,6.9,12.1,8);
INSERT INTO `weight_limit` (`id`, `week`,`weight_max`,`weight_min`,`bmi`) VALUES (310,30,7.3,12.7,8);
INSERT INTO `weight_limit` (`id`, `week`,`weight_max`,`weight_min`,`bmi`) VALUES (311,31,7.7,13.4,8);
INSERT INTO `weight_limit` (`id`, `week`,`weight_max`,`weight_min`,`bmi`) VALUES (312,32,8.1,14,8);
INSERT INTO `weight_limit` (`id`, `week`,`weight_max`,`weight_min`,`bmi`) VALUES (313,33,8.5,14.6,8);
INSERT INTO `weight_limit` (`id`, `week`,`weight_max`,`weight_min`,`bmi`) VALUES (314,34,8.9,15.3,8);
INSERT INTO `weight_limit` (`id`, `week`,`weight_max`,`weight_min`,`bmi`) VALUES (315,35,9.3,15.9,8);
INSERT INTO `weight_limit` (`id`, `week`,`weight_max`,`weight_min`,`bmi`) VALUES (316,36,9.7,16.5,8);
INSERT INTO `weight_limit` (`id`, `week`,`weight_max`,`weight_min`,`bmi`) VALUES (317,37,10.1,17.2,8);
INSERT INTO `weight_limit` (`id`, `week`,`weight_max`,`weight_min`,`bmi`) VALUES (318,38,10.5,17.8,8);
INSERT INTO `weight_limit` (`id`, `week`,`weight_max`,`weight_min`,`bmi`) VALUES (319,39,10.9,18.4,8);
INSERT INTO `weight_limit` (`id`, `week`,`weight_max`,`weight_min`,`bmi`) VALUES (320,40,11.3,19.1,8);

--
-- Initialize for table user_bmi
--

CREATE TABLE IF NOT EXISTS  `user_bmi` (
    `id` bigint  NOT NULL AUTO_INCREMENT,
	`twins` tinyint NOT NULL,
    `current_weight` double NOT NULL,
    `weight_before` double NOT NULL,
    `height` int NOT NULL,
    `bmi` double NOT NULL,
    `user_id` bigint NOT NULL,
    `week` int,
    PRIMARY KEY (`id`),
	FOREIGN KEY (`user_id`) REFERENCES `user`(id)
)ENGINE=InnoDB DEFAULT CHARSET=utf8; 

--
-- Initialize for table weight_limit
--



--
-- Initialize for table gynecological_tests
--

INSERT INTO `gynecological_tests` (`id`, `week`,`test_title`,`test_details`, `yourself_info`,`yourbaby_info`,`video`) VALUES ('1',' 1','Δεν χρειάζεται να κάνετε ακόμη κάποια εξέταση.','Είναι πολύ νωρίς ακόμη για κάποια εξέταση.', 'Στην πρώτη εβδομάδα κύησης έχετε ακόμη την περίοδο σας. Αυτή η εβδομάδα θεωρείται και τυπικά η πρώτη εβδομάδα της εγκυμοσύνης αν και σε αυτή τη φάση δεν είστε ακόμη έγκυες. Είναι εξαιρετικά δύσκολο  να βρεθεί η ακριβής στιγμή που το σπερματοζωάριο συναντά το ωάριο αφού μπορεί τα σπερματοζωάρια του συντρόφου σας να ζουν αρκετές μέρες στο σώμα σας μέχρι τη γονιμοποίηση. Γι αυτό και η πρώτη μέρα της τελευταίας σας περιόδου αποτελεί το σημείο έναρξης των σαράντα εβδομάδων που διαρκεί η εγκυμοσύνη.','Τη στιγμή της σύλληψης καθορίζεται το φύλο του μωρού σας χάρη στα χρωμοσώματα Χ και Ψ που βρίσκονται στο σπέρμα του πατέρα.','https://www.youtube.com/watch?v=0DgfGi_4rAA');

INSERT INTO `gynecological_tests` (`id`, `week`,`test_title`,`test_details`, `yourself_info`,`yourbaby_info`,`video`) VALUES ('2', '2','Δεν χρειάζεται να κάνετε ακόμη κάποια εξέταση.','Είναι πολύ νωρίς ακόμη για κάποια εξέταση.', 'Την 2η εβδομάδα ο οργανισμός προετοιμάζεται για την ωορρηξία. Η επίστρωση της μήτρας παχαίνει και τα ωοθυλάκια ωριμάζουν μέχρι να επικρατήσει το ένα που θα γονιμοποιηθεί. Μέσα στο ωοθυλάκιο περιμένει ένα ωάριο ή και  δύο όταν πρόκειται για μη μονοωγενή δίδυμα. Κατά τις δύο πρώτες εβδομάδες της εγκυμοσύνης δεν έχουμε ακόμα σύλληψη και ανάπτυξη του εμβρύου. Η πραγματική ηλικία του εμβρύου αρχίζει να «μετράει» από την τρίτη εβδομάδα οπότε έχουμε τη σύλληψη και την εμφύτευση του γονιμοποιημένου ωαρίου στη μήτρα. Οι αλλαγές που θα παρατηρείτε μέρα με τη μέρα στο σώμα σας οφείλονται στην επίδραση διάφορων ορμονών, που εκκρίνονται από τους αδένες, τις ωοθήκες και τον πλακούντα. Η εντατική δράση τους στη συνέχεια είναι αυτή που στηρίζει την εγκυμοσύνη και εξασφαλίζει την υγιή ανάπτυξη του εμβρύου. Οι ορμόνες προετοιμάζουν επίσης τη μήτρα, τη λεκάνη και τους μαστούς, για να ανταποκριθούν στον τοκετό και το θηλασμό. Με την πρόοδο της εγκυμοσύνης, καθώς το σώμα θα βρίσκει μια νέα ορμονική ισορροπία, τα συμπτώματα αυτά υποχωρούν.',null,'https://www.youtube.com/watch?v=0DgfGi_4rAA');

INSERT INTO `gynecological_tests` (`id`, `week`,`test_title`,`test_details`, `yourself_info`,`yourbaby_info`,`video`) VALUES ('3', '3','Δεν χρειάζεται να κάνετε ακόμη κάποια εξέταση.','Είναι πολύ νωρίς ακόμη για κάποια εξέταση.', 'Τα σερπατοζωάρια ταξιδεύουν προς τις σάλπιγγες.Εκατομμύρια σπερματοζωάρια πολιορκούν το ωάριο.Ένα απο αυτά πέτυχε το σκοπό του!Απο το ωάριο και το σπερματοζωάριο δημιουργείται ένα κοινό κύτταρο,ο Ζυγωττής.Ο Ζυγωτής πολλαπλασιάζεται και τα κύτταρα γίνονται 2,4,8,16 δημιουργώντας τη βλαστοκύστη. Η βλαστοκύστη φθάνει απο τις σάλπιγγες στη μήτρα.Συγχαρητήρια είστε 3 εβδομάδων έγκυος. Μετά την ωορρηξία περίπου στα μέσα του κύκλο σας, ένα ωάριο γλιστράει στις σάλπιγγες και γονιμοποιείται από το πιο δυνατό σπερματοζωάριο που κατάφερε να νικήσει  εκατομμύρια “ανταγωνιστές” του. Το γονιμοποιημένο ωάριο κλείνει και αρχίζει να διαιρείται, για να μετατραπεί σε μια μικροσκοπική μπάλα από κύτταρα.Το μέγεθος του είναι περίπου όσο το κεφάλι μιας καρφίτσας. Αυτήν την εβδομάδα η βλαστοκύστη όπως τώρα λέγεται αρχίζει την πορεία της από τη σάλπιγγα στη μήτρα όπου θα εμφυτευθεί για να μεγαλώσει. Μπορεί να παρατηρήσετε λίγες κηλίδες αίμα. Δεν υπάρχει λόγος ανησυχίας.  Στην πραγματικότητα είναι και από τα πρώτα σημάδια επιτυχίας! Σε κάποιες γυναίκες ξεκινούν κάποια συμπτώματα εγκυμοσύνης ενώ σε κάποιες άλλες αργούν λίγο ακόμη. Θυμηθείτε κάθε εγκυμοσύνη είναι διαφορετική.','Τη στιγμή της σύλληψης καθορίζεται το φύλο του μωρού σας χάρη στα χρωμοσώματα Χ και Ψ που βρίσκονται στο σπέρμα του πατέρα.','https://www.youtube.com/watch?v=0DgfGi_4rAA');

INSERT INTO `gynecological_tests` (`id`, `week`,`test_title`,`test_details`, `yourself_info`,`yourbaby_info`,`video`) VALUES ('4', '4','Ηπερηχογράφημα επιβεβαίωσης κυήσεως.','Έχετε κάνει τεστ εγκυμοσύνης;Συμβουλευτείτε τον ιατρό σας εάν θα πρέπει να υποβληθείτε σε Υπερηχογράφημα Επιβεβαίωσης Κυήσεως', 'Μόλις εμφυτευτεί το κύημα στη μήτρα, απελευθερώνει ουσίες, που προκαλούν τη διακοπή της περιόδου και προετοιμάζουν το σώμα για την επερχόμενη εγκυμοσύνη. "Ξεγελούν", επίσης, το ανοσοποιητικό σύστημα,  ώστε να δεχθεί το έμβρυο και να μην το αποβάλλει ως ξένο σώμα. Για τις περισσότερες γυναίκες το πρώτο σημάδι εγκυμοσύνης είναι η καθυστέρηση. Εάν μέχρι αυτήν την εβδομάδα δεν έχετε την περιοδό σας τότε υπάρχει μεγάλη πιθανότητα να είστε έγκυες. Με την εμφύτευση του εμβρύου στη μήτρα η καρδιά σας αρχίζει να χτυπάει πιο γρήγορα (περίπου 15 χτύποι παραπάνω το λεπτό).Για αυτό είναι πιθανόν να αισθάνεστε κουρασμένες κατά τη διάρκεια της μέρας. Πάντως τα συμπτώματα εγκυμοσύνης διαφέρουν από γυναίκα σε γυναίκα!','Αυτήν την εβδομάδα  το κυκλοφορικό σύστημα μαζί με την καρδιά (όχι μεγαλύτερη από έναν σπόρο παπαρούνας) είναι από τα πρώτα που αρχίζουν να λειτουργούν. Επίσης αρχίζει να σχηματίζεται το κεντρικό νευρικό σύστημα (ο εγκέφαλος και ο νωτιαίος μυελός), οι μύες και τα οστά, αν και σε αυτό το σημείο το έμβρυο μοιάζει  περισσότερο με έναν γυρίνο από ότι με έναν άνθρωπο.  Ο νευρικός σωλήνας, που συνδέει τον εγκέφαλο και το νωτιαίο μυελό,είναι ακόμη ανοιχτός αλλά μέχρι την επόμενη εβδομάδα θα κλείσει. Το έμβρυο έχει τώρα το μέγεθος ενός κουκουτσιού από πορτοκάλι ή ενός σπόρου από σουσάμι.Το μέγεθος του κυμαίνεται στο 0,36 - 1 χιλιοστό','https://www.youtube.com/watch?v=0DgfGi_4rAA');

INSERT INTO `gynecological_tests` (`id`, `week`,`test_title`,`test_details`, `yourself_info`,`yourbaby_info`,`video`) VALUES ('5', '5','Ηπερηχογράφημα επιβεβαίωσης κυήσεως.','Έχετε κάνει τεστ εγκυμοσύνης;Συμβουλευτείτε τον ιατρό σας εάν θα πρέπει να υποβληθείτε σε Υπερηχογράφημα Επιβεβαίωσης Κυήσεως','Τα συμπτώματα εγκυμοσύνης συνεχίζονται ή αρχίζουν αυτήν την εβδομάδα. Οι περισσότερες γυναίκες παρατηρούν ναυτία, κούραση, και συχνοουρία. Είναι μύθος ότι η ναυτία είναι πρωινή υπόθεση. Πολλές φορές παρατηρείται και στο τέλος της μέρας. Το στήθος αρχίζει να γίνεται πιο ευαίσθητο ενώ κάποιες γυναίκες έχουν πονοκεφάλους κυρίως στο πρώτο τρίμηνο της εγκυμοσύνης.','Αυτήν την εβδομάδα  το κυκλοφορικό σύστημα μαζί με την καρδιά (όχι μεγαλύτερη από έναν σπόρο παπαρούνας) είναι από τα πρώτα που αρχίζουν να λειτουργούν. Επίσης αρχίζει να σχηματίζεται το κεντρικό νευρικό σύστημα (ο εγκέφαλος και ο νωτιαίος μυελός), οι μύες και τα οστά, αν και σε αυτό το σημείο το έμβρυο μοιάζει  περισσότερο με έναν γυρίνο από ότι με έναν άνθρωπο.  Ο νευρικός σωλήνας, που συνδέει τον εγκέφαλο και το νωτιαίο μυελό, είναι ακόμη ανοιχτός αλλά μέχρι την επόμενη εβδομάδα θα κλείσει. Το έμβρυο έχει τώρα το μέγεθος ενός κουκουτσιού από πορτοκάλι ή ενός σπόρου από σουσάμι.Το μέγεθος του κυμαίνεται στο 1.25','https://www.youtube.com/watch?v=0DgfGi_4rAA');

INSERT INTO `gynecological_tests` (`id`, `week`,`test_title`,`test_details`, `yourself_info`,`yourbaby_info`,`video`) VALUES ('6', '6','Έλεγχος καρδιακής λειτουργίας','Συμβουλευτείτε τον ιατρό σας εάν θα πρέπει να υποβληθείτε σε Υπερηχογράφημα Ελέγχου Καρδιακής Λειτουργίας του εμβρύου και εξετάσεις Προγεννητικού Ελέγχου.','Η παραγωγή ορμόνων εγκυμοσύνης (hCG) συνεχίζει να αυξάνεται,κάνοντάς σας επιρεπή σε συπτώματα ναυτίας και κόπωσης.Η πίεση στο αίμα σας είναι χαμηλότερη απο ότι ήταν πρίν μείνετε έγκυος με συνέπεια να αισθάνεστε ζαλάδες.Η παραγωγή επιπλέον προγεστερόνης,μπορεί να σας κάνει να αισθάνεστε κόπωση και ναυτία. Παρά τα παραπάνω συμπτώματα δεν χρειάζεται να σταματήσετε την εξάσκηση ή να περιορίσετε τις δραστηριότητές σας, εκτός έαν το επιθυμείτε εσείς.Στην πραγματικότητα το να μείνετε δραστήρια θα βοηθήσει το σώμα σας να είναι σε θέση να αντιμετωπίσει το άγχος των επιπλέων κιλών της εγκυμοσύνης.','Η καρδιά αρχίζει να αντλεί αίμα και ο νευρικός σωλήνας που θα γίνει η σπονδυλική στήλη κλείνει (για αυτό είναι απαραίτητο να παίρνετε νωρίς το φολικό οξύ).Το έμβρυο παίρνει ένα σχήμα μισοφέγγαρου. Τα χέρι και τα  πόδια αρχίζουν να σχηματίζονται και το δέρμα είναι ημιδιαφανές.Η καρδιά του εμβρύου έχει αρχίσει να χτυπά αλλά είναι αδύνατο να την αφουγκραστείτε.Το μήκος του κυμαίνεται μεταξύ 4 έως 6 χιλιοστά.','https://www.youtube.com/watch?v=0DgfGi_4rAA');

INSERT INTO `gynecological_tests` (`id`, `week`,`test_title`,`test_details`, `yourself_info`,`yourbaby_info`,`video`) VALUES ('7', '7','Έλεγχος καρδιακής λειτουργίας','Συμβουλευτείτε τον ιατρό σας εάν θα πρέπει να υποβληθείτε σε Υπερηχογράφημα Ελέγχου Καρδιακής Λειτουργίας του εμβρύου και εξετάσεις Προγεννητικού Ελέγχου.', 'Ήρθε η ώρα για το πρώτο σας υπερηχογράφημα.Στο υπερηχογράφημα αυτό ελέγχεται εάν η εγκυμοσύνη είναι ενδομήτρια και ακούτε για πρώτη φορά την καρδιά του μωρού σας.Με αυτό το υπερηχογράφημα καθορίζεται καριβώς η ημερομηνία της κύησης καθώς και η πιθανή ημερομηνία τοκετού.Η βλέννα στον τράχηλό σας είναι παχιά, σχηματίζοντας ένα βύσμα που θα κρατήσει τη μήτρα σας σφραγισμένη μέχρι να γεννήσετε.Μπορεί να μην παρατηρήσετε διαφορές στο σώμα σας ή ίσως να παρατηρήσετε ότι αρχίζετε να χάνετε τη μέση σας.Μπορεί να αισθάνεστε πως έχετε πολλές φορές ναυτία, ή ίσως να μην αισθάνεστε κάτι τέτοιο.Η έλλειψη συμπτωμάτων δεν σημαίνει ότι υπάρχει κάποιο πρόβλημα με εσάς ή το μωρό. Εάν αισθάνεστε ήδη αδιάθετη, μπορεί να σας διευκολύνει να ξέρετε ότι πολλά συμπτώματα σημαίνουν ότι οι ορμόνες εγκυμοσύνης εργάζονται σκληρά για να υποστηρίξουν την εγκυμοσύνη σας.','Το κεφάλι του μωρού σας είναι περίπου το 1/3 του μεγέθους ολόκληρου του εμβρύου του.Το μυαλό και το πρόσωπο αναπτύσονται ταχύτατα.Τα ρουθούνια και οι φακού των ματιών αρχίζουν να σχηματίζονται.Το ίδιο και τα χέρια τα οποία έχουν μήκος περίπου 1/3 ίντσας.Το μήκος του εμβρύου κυμαίνεται μεταξύ 5-9 χιλιοστών.','https://www.youtube.com/watch?v=0DgfGi_4rAA');

INSERT INTO `gynecological_tests` (`id`, `week`,`test_title`,`test_details`, `yourself_info`,`yourbaby_info`,`video`) VALUES ('8', '8','Έλεγχος καρδιακής λειτουργίας','Συμβουλευτείτε τον ιατρό σας εάν θα πρέπει να υποβληθείτε σε Υπερηχογράφημα Ελέγχου Καρδιακής Λειτουργίας του εμβρύου και εξετάσεις Προγεννητικού Ελέγχου.', 'Η περιφέρεια σας μπορει να ξεκινήσει να "μεγαλώνει" και στο πρόσωπό σας να εμφανιστούν οι πρώτες πανάδες.Εάν η εγκυμοσύνη σας συνοδεύεται μέχρι στιγμής με ναυτία είναι πιθανόν το μωρό σας να αρχίσει να κλωτσάει απο εδώ και στο εξής.Οι ερευνητές δεν γνωρίζουν την ακριβή αιτία που συμβαίνει,αλλά σίγουρα σχετίζεται με τις ογκογόνες ορμόνες σας.Είναι συνηθησμένο σε αυτό το στάδιο να έχετε οξύ πόνο και στις δύο πλευρές της λεκάνης σας, ειδικά όταν στρίβετε ή σηκώνεστε.Η μήτρα σας γίνεται βαριά, και αυτό μπορεί να τεντώσει τους στρογγυλούς συνδέσμους σας,δηλαδή τους μύες που κρατούν τη μήτρα σας στη θέση της.','Οι πνεύμονες, το στομάχι,το ήπαρ και το πάγκρεας αρχίζουν να σχηματίζονται.Δημιουγούνται η ρινική κοιλότητα,η παρεγκεφαλίδα,ο σπλήνας και το άνω χείλος.Το σώμα αρχίζει να ισιώνει και ξεκινούν οι πρώτες κινήσεις. Το μήκος του μωρού σας είναι περίπου 9-13 χιλιοστά.','https://www.youtube.com/watch?v=0DgfGi_4rAA');

INSERT INTO `gynecological_tests` (`id`, `week`,`test_title`,`test_details`, `yourself_info`,`yourbaby_info`,`video`) VALUES ('9', '9','Αυχενική διαφάνεια  PAPP-A & CVS','Η επόμενη εξέτσαση που πρέπει να κάνετε είναι η Μέτρηση Αυχενικής Διαφάνειας και PAPP-A.Συμβουλευτείτε τον ιατρό σας εάν θα πρέπει να υποβληθείτε σε Λήψη Τροφοβλάστης (CVS)', 'Τα επίπεδα hCG που προκαλούν αλλεργίες βρίσκονται στο αποκορύφωμά τους αυτήν την εβδομάδα. Τα καλά νέα είναι ότι από την επόμενη εβδομάδα, καθώς τα ορμονικά σας επίπεδα σταθεροποιούνται, θα αρχίσετε να αισθάνεστε πολύ καλύτερα. Τα κακά νέα είναι ότι αυτήη εβδομάδα πιθανότατα θα είναι δύσκολη.Η μήτρα σας έχει διπλασιαστεί και τώρα έχει μέγεθος περίπου μιας μπάλας του τένις.Η περιοχή κάτω από τον ομφαλό σας είναι σίγουρα σταθερότερη από το συνηθισμένο.Οι περισσότερες γυναίκες αναφέρουν ότι δεν ενδιαφέρονται για το σεξ σε αυτό το στάδιο, αν και ορισμένες γυναίκες αναφέρουν επίσης ότι ενδιαφέρονται περισσότερο από το συνηθισμένο. Μπορεί να αρχίσετε να παρατηρείτε αλλαγές στα μαλλιά και το δέρμα σας.Επίσης μπορεί να μην έχετε διάθεση για να αθληθείτε αυτή την εβδομάδα αλλα θα πρέπει να προσπαθήσετε να κάνετε τουλάχιστον περιπάτους.Αυτό θα σας βοηθήσει στο να χωνεύεται πιο εύκολα.','Η καρδιά είναι σχεδόν εξ ολοκλήρου αναπτυγμένη. Τα βλέφαρα σχηματίζονται, όπως και οι θύλακες των τριχών.Τα δάχτυλα και οι πατουσες δεν έχουν πλέον ιστό. Τα χέρια αναπτύσσουν οστά και αρχίζουν να αγγίζουν το πρόσωπο του.Τα πόδια αρχίζουν να κινούνται. Μήκος: περίπου 18 χιλιοστά.','https://www.youtube.com/watch?v=0DgfGi_4rAA');

INSERT INTO `gynecological_tests` (`id`, `week`,`test_title`,`test_details`, `yourself_info`,`yourbaby_info`,`video`) VALUES ('10', '10','Αυχενική διαφάνεια  PAPP-A & CVS', 'Η επόμενη εξέτσαση που πρέπει να κάνετε είναι η Μέτρηση Αυχενικής Διαφάνειας και PAPP-A.Συμβουλευτείτε τον ιατρό σας εάν θα πρέπει να υποβληθείτε σε Λήψη Τροφοβλάστης (CVS)','Τα μαλλιά σας και τα νύχια σας αρχίζουν να δυναμώνουν πολύ, εξαιτίας των βιταμινών που λαμβάνετε. Η μήτρα σας μεγαλώνει ακόμη περισσότερο και η εμφάνιση σας προδίδει την εγκυμοσύνη σας. Η όρεξη σας αρχίζει και βελτιώνεται καθώς οι ναυτίες αρχίζουν και μειώνονται αν και ακόμη είναι εξαιρετικά ενοχλητικές  κάποιες μυρωδιές.','Τα ζωτικά όργανα του μωρού σας αρχίζουν να λειτουργούν όλα μαζί. Αυτήν την εβδομάδα σχηματίζονται επίσης οστά και χόνδροι καθώς και μικρές οδοντώσεις στα πόδια που θα εξελιχθούν σε γόνατα και αστραγάλους.  Οι ώμοι στα χέρια του ήδη λειτουργούν και είναι σε θέση να καταπίνει και να κλωτσά. Επίσης  την 10η εβδομάδα εξαφανίζεται η ουρά και τελειώνει πλέον η εμβρυονική περίοδος ανάπτυξης.  Το μωράκι σας έχει αρχίσει πλέον να αποκτά τα ανθρώπινα χαρακτηριστικά του. Οι νεφροί παράγουν μεγαλύτερες ποσότητες ούρων και αν το μωρό σας είναι αγόρι οι όρχεις του παράγουν τεστοστερόνη. Το έμβρυο έχει πλέον το μέγεθος ενός δαμάσκηνου.Μήκος περίπου 3-4 εκατοστά και βάρος 4-5 γραμμάρια','https://www.youtube.com/watch?v=s9U0TvkQKmg');

INSERT INTO `gynecological_tests` (`id`, `week`,`test_title`,`test_details`, `yourself_info`,`yourbaby_info`,`video`) VALUES ('11', '11','Αυχενική διαφάνεια  PAPP-A & CVS', 'Ήρθε η στιγμή να κάνετε τη Μέτρηση Αυχενικής Διαφάνειας και PAPP-A.Συμβουλευτείτε τον ιατρό σας εάν θα πρέπει να υποβληθείτε σε Λήψη Τροφοβλάστης (CVS)','Καθώς πλησιάζετε στο τέλος του πρώτου τριμήνου η εγκυμοσύνη σας θεωρείται πιο ασφαλής καθώς το μωρό σας έχει πλέον ξεπεράσει την κρίσιμη περίοδο της ανάπτυξης του. Το βάρος έχει αρχίσει να αυξάνεται και από αυτήν την εβδομάδα σε κάποιες γυναίκες η εγκυμοσύνη αρχίζει να φαίνεται. Σταδιακά θα δείτε το σώμα σας να αλλάζει και μπορεί να παρατηρήσετε ότι η μέση σας έχει αρχίσει να μεγαλώνει. Οι ορμόνες σας, σας κάνουν ακόμη πιο ευαίσθητες και ευσυγκίνητες γι αυτό προετοιμαστείτε τόσο εσείς όσο και ο σύντροφός σας.','Το πιο κρίσιμο κομμάτι στην ανάπτυξη του εμβρύου έχει πλέον τελειώσει γιατί κάποια στιγμή κατά τη διάρκεια αυτής της εβδομάδας το αίμα αρχίζει να κυκλοφορεί ανάμεσα στο μωρό και τη μήτρα και ο πλακούντας αρχίζει να λειτουργεί. Το κεφάλι του είναι σχεδόν όσο το μισό κορμί του και αυτό αλλάζει όσο το έμβρυο μεγαλώνει. Από αυτήν την εβδομάδα μέχρι και την εικοστή η ανάπτυξη του μωρού σας είναι ραγδαία καθώς μέσα σε αυτές τις 9 εβδομάδες θα αυξήσει 30 φορές το βάρος του και θα σχεδόν θα τριπλασιάσει το ύψος του. Οι θήλακες για τα μαλλιά σχηματίζονται και οι ρίζες από τα νύχια των χεριών και των ποδιών αναπτύσσονται.  Αν είναι κορίτσι τώρα αναπτύσσονται οι ωοθήκες.  Το έμβρυο έχει τώρα το μέγεθος ενός μεγάλου σύκου.Το μέγεθος του εμβρύου κυμαίνεται στα 4 εκατοστά και το βάρος του στα 8 γραμμάρια.','https://www.youtube.com/watch?v=s9U0TvkQKmg');

INSERT INTO `gynecological_tests` (`id`, `week`,`test_title`,`test_details`, `yourself_info`,`yourbaby_info`,`video`) VALUES ('12', '12','Αυχενική διαφάνεια  PAPP-A & CVS','Ήρθε η στιγμή να κάνετε τη Μέτρηση Αυχενικής Διαφάνειας και PAPP-A.Συμβουλευτείτε τον ιατρό σας εάν θα πρέπει να υποβληθείτε σε Λήψη Τροφοβλάστης (CVS)', 'Οι ναυτίες και οι εμετοί δίνουν τη θέση τους στη ζωτικότητα και την ενέργεια που αρχίζετε πλέον να αισθάνεστε. Ακόμη κι αν η αδιαθεσία σας συνεχίζετε κάντε λίγο υπομονή αφού σύντομα θα ανακτήσετε την όρεξη σας. Το άγχος σας σαφώς μειώνεται αφού ο κίνδυνος της αποβολής δεν είναι πλέον ορατός. Τα καλά νέα είναι ότι η κύστη σας δεν πιέζεται τόσο οπότε η συχνοουρία δεν είναι πολύ συχνή.  Από το τρίτο τρίμηνο ωστόσο  η μήτρα σας θα αρχίζει πάλι να πιέζει την κύστη, επειδή το έμβρυο θα έχει μεγαλώσει, οπότε τα πράγματα θα δυσκολέψουν.Συμβουλή:Σιγουρευτείτε ότι οποιοδίποτε κρέας καταναλώνετε είναι πολύ καλά μαγειρεμένο για να μειώσει οποιαδήποτε πιθανότητα τοξοπλάσμωσης.Καταναλώνετε 8-10 ποτήρια νερό ημερησίως για να αποφευχθούν όσο το δυνατό οι κυστίτιδες.','Αν και τα περισσότερα συστήματα του εμβρύου έχουν σχηματιστεί πλήρως πρέπει να ωριμάσουν πολύ ακόμα. Το σώμα του μωρού σας δουλεύει σκληρά για την ανάπτυξη του και το πρόσωπό του αλλάζει μοιάζοντας περισσότερο ανθρώπινο. Τα μάτια, που στην αρχή βρίσκονταν στις πλευρές του κεφαλιού του, έχουν πλησιάσει πιο κοντά μεταξύ τους και τα αυτιά είναι σχεδόν στην κανονική τους θέση. Τα δάχτυλα των χεριών και των ποδιών έχουν διαχωριστεί . Τα μαλλιά και τα νύχια αρχίζουν να μεγαλώνουν. Το πεπτικό σύστημα αρχίζει να κάνει συσταλτικές κινήσεις οπότε το μωρό σας θα είναι σε θέση να τρώει. Ο μυελός των οστών παράγει λευκά αιμοσφαίρια και η υπόφυση στη βάση του εγκεφάλου αρχίζει να παράγει ορμόνες. Το έμβρυο έχει τώρα το μέγεθος ενός δαμάσκηνου.Μήκος περίπου 6 εκατοστά και βάρος 15 γραμμάρια.','https://www.youtube.com/watch?v=s9U0TvkQKmg');

INSERT INTO `gynecological_tests` (`id`, `week`,`test_title`,`test_details`, `yourself_info`,`yourbaby_info`,`video`) VALUES ('13', '13','Αυχενική διαφάνεια  PAPP-A & CVS','Ήρθε η στιγμή να κάνετε τη Μέτρηση Αυχενικής Διαφάνειας και PAPP-A.Συμβουλευτείτε τον ιατρό σας εάν θα πρέπει να υποβληθείτε σε Λήψη Τροφοβλάστης (CVS)','Είστε επίσημα στο δεύτερο τρίμηνο κύησης. Αυτά είναι καταπληκτικά νέα για δύο λόγους. Πρώτον, ο πιθανός κίνδυνος αποβολής έχει μειωθεί σημαντικά, και δεύτερον, πολλές γυναίκες βλέπουνε τα πρώτα συμπτώματα της εγκυμοσύνης να υποχωρούν (όπως η ναυτία), ίσως βελτίωση στη libido σας καθώς και αύξηση στα επίπεδα ενέργειας. Προφανώς έχετε χάσει και την ωραία σας μέση για αυτό είναι καιρός να φοράτε ρούχα εγκυμοσύνης!','Η μήτρα σας είναι αρκετά μεγάλη τώρα και είναι εμφανές ότι γεμίζει την πύελο κι αρχίζει να αναπτύσσεται προς το επάνω μέρος της κοιλιάς.   Από τώρα μέχρι και την 24η εβδομάδα κύησης, το μωράκι σας αναπτύσσεται ιδιαιτέρως εντυπωσιακά.  Παρά τις μικρές αναλογίες του, το μωρό σας είναι πλήρως διαμορφωμένο με μοναδικά δακτυλικά αποτυπώματα. Επίσης ενδιαφέρον έχει η σχετική μείωση στο ρυθμό ανάπτυξης του κεφαλιού, συγκριτικά με το ρυθμό ανάπτυξης του σώματος του μωρού σας.  Τα ανθρώπινα χαρακτηριστικά του διαμορφώνονται ταχύτατα, έτσι τα μάτια έρχονται στο μπροστινό μέρος του κεφαλιού και τα αυτιά στα πλάγια.  Τα έντερα αναπτύσσονται αρχικά μέσα στον ομφάλιο λώρο, έξω από το σώμα του, αλλά σε αυτό το στάδιο τραβιούνται μέσα στην κοιλότητα της κοιλιάς του εμβρύου.Μήκος περίπου 8 εκατοστά και βάρος 15-25 γραμμάρια.','https://www.youtube.com/watch?v=s9U0TvkQKmg');

INSERT INTO `gynecological_tests` (`id`, `week`,`test_title`,`test_details`, `yourself_info`,`yourbaby_info`,`video`) VALUES ('14', '14','A-TEST & Αμνιοπαρακέντησης', 'Αν δεν έχετε κάνει Μέτρηση Αυχενικής Διαφάνειας και PAPP-A η επόμενη εξέταση που μπορείτε να υποβληθείτε είναι το A-Test.Συμβουλευτείτε τον ιατρό σας εάν θα πρέπει να κάνετε αμνιοπαρακέντηση.','Έχετε πιθανόν πολύ καλύτερη διάθεση και περισσότερη ενέργεια απο ότι τους πρώτους τρεις μήνες της εγκυμοσύνης σας, και έχετε αφήσει τα πρώτα  συμπτώματα της εγκυμοσύνης για τα καλά στο παρελθόν. Σε κάποιες γυναίκες ένα μικρό φουσκωματάκι στην κοιλιά εχει αρχίσει ήδη να φαίνεται. Επίσης αν νιώθετε κάποια τραβήγματα χαμηλά μην πανικοβάλλεστε καθώς τα όργανα σας μετακινούνται προκειμένου να κάνουν χώρο για το μωρό σας. Επίσης μερικές άτυχες γυναίκες διαπιστώνουν ότι η ναυτία και σε αυτήν την εβδομάδα συνεχίζεται.  Άν είστε μία από αυτές, επικοινωνήστε με τον γυναικολόγο σας για περισσότερες συμβουλές.Τέλος εάν είστε πάνω απο 35 ετών, πλησιάζει η χρονική στιγμή για την αμνιοπαρακέντηση.Η αμνιοπαρακέντηση παρέχει γεμετικές πληροφορίες για το έμβρυο όπως και πληροφορίες για την υγεία του και το πώς αναπτύσσεται, μέσω της αφαίρεσης ενός μικρού δείγματος αμνιακού υγρού.','Τα μαλλιά είναι το μεγάλο θέμα  της εβδομάδας! Το μωρό σας αποκτάει πολλά μαλλάκια, όχι μόνο στο κεφάλι και στα φρύδια του αλλά σε ολόκληρο το σώμα του. Αυτό το  λεπτό στρώμα χνουδιού, το αποκαλούμενο lanugo, εξαφανίζεται συνήθως λίγο πριν την γέννηση. Επίσης, μερικοί μύες αρχίζουν να λειτουργούν και αυτή την εβδομάδα το μωρό σας μπορεί να πιάσει, να κάνει γκριμάτσες, ακόμη και να δείξει με τον αντίχειρα του καθώς τώρα μαθαίνει αντανακλαστικά να χρησιμοποιεί τα χέρια του. Ο λαιμός συνεχίζει να μακραίνει και το πιγούνι δεν ακουμπά πια στο στέρνο. Γύρω στην 14η εβδομάδα το μωρό σας αναπτύσσεται περισσότερο από κάθε άλλη εβδομάδα. Το έμβρυο σας έχει τώρα το μέγεθος ενός λεμονιού.Μήκος περίπου 9 εκατοστά και βάρος 25-40 γραμμάρια.','https://www.youtube.com/watch?v=s9U0TvkQKmg');

INSERT INTO `gynecological_tests` (`id`, `week`,`test_title`,`test_details`, `yourself_info`,`yourbaby_info`,`video`) VALUES ('15', '15','A-TEST & Αμνιοπαρακέντησης', 'Αν δεν έχετε κάνει Μέτρηση Αυχενικής Διαφάνειας και PAPP-A η επόμενη εξέταση που μπορείτε να υποβληθείτε είναι το A-Test.Συμβουλευτείτε τον ιατρό σας εάν θα πρέπει να κάνετε αμνιοπαρακέντηση.','Με το μωράκι να μεγαλώνει μέσα σας, ο ύπνος αρχίζει και γίνεται δύσκολη υπόθεση!  Επίσης μια από τις πιο συναρπαστικές στιγμές της εγκυμοσύνης είναι όταν αισθάνεστε το μωράκι σας να κουνιέται. Οι περισσότερες γυναίκες παρατηρούν τα σκιρτήματα, όπως αποκαλούνται, μεταξύ της 16ης και της 20ης εβδομάδας.  Είναι λίγο νωρίς ακόμη αλλά μπορεί να αισθανθείτε τις πρώτες κινήσεις χαμηλά στην κοιλιά σας κάτι σαν "πετάρισμα" !','Το μωρό σας είναι ακόμη τόσο μικρό που χωράει  στην παλάμη του χεριού σας. Οι μυς του συνεχίζουν να αναπτύσσονται, όπως επίσης και τα δακτυλικά του αποτυπώματα. Το νέο της εβδομάδας, είναι οι λόξιγκες, ένας πρόδρομος της αναπνοής. Δεν μπορείτε να τους ακούσετε επειδή ο οργανισμός του είναι γεμάτος υγρά  παρά με αέρα αλλά μην εκπλαγείτε εάν τους αισθανθείτε αργότερα. Το δέρμα του είναι λεπτό και ξεχωρίζουν τα αιμοφόρα αγγεία κάτω από το δέρμα του. Το σωματάκι του εξακολουθεί να καλύπτεται από ένα λεπτό χνούδι.  Μην εκπλαγείτε εάν στο υπερηχογράφημα το δείτε να πιπιλάει τον αντίχειρά του! Αυτήν την εβδομάδα το μωρό αρχίζει να ακούει ήχους για πρώτη φορά.Μήκος περίπου 10 εκατοστά και βάρος 50-60 γραμμάρια.','https://www.youtube.com/watch?v=0bmH4q8PWnY');

INSERT INTO `gynecological_tests` (`id`, `week`,`test_title`,`test_details`, `yourself_info`,`yourbaby_info`,`video`) VALUES ('16', '16','A-TEST & Αμνιοπαρακέντησης', 'Αν δεν έχετε κάνει Μέτρηση Αυχενικής Διαφάνειας και PAPP-A η επόμενη εξέταση που μπορείτε να υποβληθείτε είναι το A-Test.Συμβουλευτείτε τον ιατρό σας εάν θα πρέπει να κάνετε αμνιοπαρακέντηση.','Το έμβρυο θα αρχίσει να κινείται πιο συχνά και έτσι πιθανόν να αρχίσετε να νιώθετε κάποιες "πεταλούδες"  στην κοιλιά σας. Το στήθος σας συνεχίζει να μεγαλώνει όπως και η μήτρα σας και τώρα ζυγίζει περίπου 250 γραμμάρια. Είναι επίσης πιθανόν να αισθάνεστε  και πόνους που προκαλούνται από τους συνδέσμους που τεντώνονται στην κοιλία σας. Αυτοί οι πόνοι είναι συνήθως προσωρινοί αλλά η ανάπτυξη της μήτρας θα δώσει πρόσθετη πίεση στη μέση σας.  Ακόμη αυξάνεται και η ποσότητα του αμνιακού υγρού που περιβάλλει το μωρό σας, περίπου στα 250 χιλιοστόλιτρα. Πολλές γυναίκες βλέπουν το βάρος τους να αυξάνται  περίπου 2.2 έως 4.5 κιλά . Μαζί με την αύξηση του βάρους παρατηρήτε  ότι σας "άνοιξε" η όρεξη. Είναι φυσιολογικό!','Μερικά από τα πιο προηγμένα λειτουργικά συστήματα τους σώματός του λειτουργούν κανονικά,  συμπεριλαμβανομένων του κυκλοφορικού και ουροποιητικού συστήματος.  Τα μάτια του μωρού σας αρχίζουν να κάνουν κάποιες  αμυδρές κινήσεις αλλά είναι ακόμη κλειστά.  Μπορεί ωστόσο να ανιχνεύσει αλλαγές στο φως από τα λεπτά του βλέφαρα. Το μωρό σας μπορεί να παίζει με το ομφάλιο λώρο (αρπάζοντας και αφήνοντας τον) ο οποίος είναι συνδεδεμένος χαμηλά με την κοιλιά του μωρού σας.  Τα νύχια στα δάκτυλα των χεριών έχουν σχηματιστεί. Το έμρυο έχει τώρα το μέγεθος ενός αχλαδιού.Μήκος περίπου 10-12 εκατοστά και βάρος 80-95 γραμμάρια','https://www.youtube.com/watch?v=0bmH4q8PWnY');

INSERT INTO `gynecological_tests` (`id`, `week`,`test_title`,`test_details`, `yourself_info`,`yourbaby_info`,`video`) VALUES ('17', '17','A-TEST & Αμνιοπαρακέντησης','Αν δεν έχετε κάνει Μέτρηση Αυχενικής Διαφάνειας και PAPP-A η επόμενη εξέταση που μπορείτε να υποβληθείτε είναι το A-Test.Συμβουλευτείτε τον ιατρό σας εάν θα πρέπει να κάνετε αμνιοπαρακέντηση.','Το βάρος συνεχίζει να αυξάνεται και πολλές γυναίκες έχουν περισσότερη ενέργεια και καλύτερη διάθεση από αυτήν την εβδομάδα. Μπορεί κάποιες να αρχίσετε να νιώθετε πόνο στην πλάτη. Η μήτρα σας έχει το μέγεθος ενός μικρού πεπονιού. Οι νέες μαμάδες αρχίζουν να αισθάνονται τις κινήσεις του μωρού τους περίπου αυτή την εβδομάδα.  Πολλές γυναίκες αναφέρουν ότι οι πρώτες αισθήσεις είναι σαν φτερούγισμα ή σαν πεταλούδισμα στο στομάχι.  Οι αληθινές  όμως κλωτσιές δεν αρχίζουν ακόμη. Μετά από ένα μήνα ή περισσότερο θα τις νιώθετε πολύ έντονα.','Πολλά πράγματα αλλάζουν αυτήν την εβδομάδα. Το μωρά σας κινείται περισσότερο  και ακούει διάφορους θορύβους. Ο χόνδρος που θα γίνει ο σκελετός του είναι στη φάση που αρχίζει να μετατρέπεται σε κόκαλο. Τα ματιά του μωρού σας κοιτάνε μπροστά και όχι στα πλάγια, τα αυτιά του είναι σχεδόν στην τελική τους θέση. Αυτή την εβδομάδα και κατά την διάρκεια των επόμενων, αρχίζει να σχηματίζεται το λίπος (ή λιπώδης ιστός) το οποίο είναι πολύ σημαντικό για την θερμοκρασία του σώματος και τον μεταβολισμό του.  Ενδέχεται να μην αισθάνεστε τις κινήσεις του μωρού σας κάθε μέρα κάτι που θα αλλάξει καθώς η εγκυμοσύνη σας προχωρά. Τo έμβρυο σας και ο πλακούντας σας έχει τώρα το ίδιο μέγεθος.Μήκος περίπου 11-12 εκατοστά και βάρος 100-140 γραμμάρια.','https://www.youtube.com/watch?v=0bmH4q8PWnY');

INSERT INTO `gynecological_tests` (`id`, `week`,`test_title`,`test_details`, `yourself_info`,`yourbaby_info`,`video`) VALUES ('18', '18','A-TEST & Αμνιοπαρακέντησης','Αν δεν έχετε κάνει Μέτρηση Αυχενικής Διαφάνειας και PAPP-A η επόμενη εξέταση που μπορείτε να υποβληθείτε είναι το A-Test.Συμβουλευτείτε τον ιατρό σας εάν θα πρέπει να κάνετε αμνιοπαρακέντηση.','Είσαστε ακριβώς μια εβδομάδα πριν από  το μισό της εγκυμοσύνης σας. Η φροντίδα της εγκυμοσύνης σας συνεχίζεται με τον ίδιο ρυθμό όπως και πριν, εκτός αν αντιμετωπίζετε κάποιο ιδιαίτερο πρόβλημα. Αλλά μην παίρνετε καταστάσεις  στα χέρια σας εάν νιώθετε αγχωμένη για κάτι.','Το βάρος του μωρού σας θα αυξηθεί 6 φορές παραπάνω τις επόμενες 4 εβδομάδες. Αυτή την εβδομάδα, μπαίνετε επίσημα στον πέμπτο μήνα της κύησης. Το στήθος του μωρού σας κάνει κινήσεις πάνω-κάτω , κάνοντας μιμητική αναπνοή χωρίς φυσικά να εισπνέει αέρα αλλά αμνιοτικό υγρό. Πιθανόν θα έχει φθάσει τα 14 εκατοστά από τη κορυφή του κεφαλιού μέχρι τους γλουτούς, και μπορεί να νιώσει αλλά και να ακούσει. Συγκεκριμένα, αυτή τη στιγμή το μόνο που μπορεί να ακούσει είναι ο χτύπος της καρδιάς σας και οι κινήσεις του εντέρου σας αλλά σύντομα θα είναι σε θέση να αντιληφθεί θορύβους έξω από την κοιλιά και να αναγνωρίσει την φωνή σας.  Παίρνει οξυγόνο από εσάς μέσω του ομφάλιου λώρου από το αίμα που περνά στον πλακούντα. Διαμέσου του πλακούντα μεταφέρονται από το αίμα σας στο αίμα του εμβρύου εκτός από οξυγόνο και θρεπτικές ουσίες, ωστόσο τα δύο κυκλοφορικά συστήματα είναι απόλυτα διαχωρισμένα.Μήκος περίπου 13-14 εκατοστά και βάρος 150 - 180 γραμμάρια.','https://www.youtube.com/watch?v=0bmH4q8PWnY');

INSERT INTO `gynecological_tests` (`id`, `week`,`test_title`,`test_details`, `yourself_info`,`yourbaby_info`,`video`) VALUES ('19', '19','A-TEST & Αμνιοπαρακέντησης','Αν δεν έχετε κάνει Μέτρηση Αυχενικής Διαφάνειας και PAPP-A η επόμενη εξέταση που μπορείτε να υποβληθείτε είναι το A-Test.Συμβουλευτείτε τον ιατρό σας εάν θα πρέπει να κάνετε αμνιοπαρακέντηση.','Το έμβρυο σας έχει μεγαλώσει καλά μέσα στην κοιλιά σας- το κεφάλι του πιθανότατα φτάνει τον ομφαλό σας. Από εδώ και πέρα θα μεγαλώνει ένα εκατοστό περίπου την εβδομάδα. Μπορεί επίσης να παρατηρήσετε κάποιες αναταραχές στην κοιλιά σας. Δεν είναι κάτι για το οποίο πρέπει να ανησυχήσετε- είναι απλώς το τέντωμα των μυών σας και των συνδέσμων που υποστηρίζουν την κοιλιά.','Συγχαρητήρια, κάνατε τον μισό δρόμο! Αυτή είναι μια σημαντική στιγμή για την ανάπτυξη των αισθήσεων του μωρού σας που λαμβάνει χώρα σε ειδικές περιοχές στον εγκέφαλο. Εάν το μωρό σας είναι κοριτσάκι, τότε έχει έξι εκατομμύρια αυγά στις ωοθήκες της. θα μειωθούν σταδιακά στο ένα εκατομμύριο όταν θα γεννηθεί.Μήκος περίπου 13-15 εκατοστά και βάρος 200 γραμμάρια.','https://www.youtube.com/watch?v=0bmH4q8PWnY');

INSERT INTO `gynecological_tests` (`id`, `week`,`test_title`,`test_details`, `yourself_info`,`yourbaby_info`,`video`) VALUES ('20', '20','Υπερηχογράφημα Β Επιπέδου','Πλησιάζει το υπερηχογράφημα Β επιπέδου.Συμβουλευτείτε τον ιατρό σας.','Έχετε πλέον φτάσει στα μέσα της εγκυμοσύνης σας. Αρχίζετε πλεόν να νιώθετε πιο ενεργητικές και λιγότερο ευέξαπτες. Έχετε πάρει κάποια κιλά αλλά είναι ακόμη πολύ νωρίς για να σας αγχώνουν.Τα ούλα σας μπορεί να γίνουν πιο ευαίσθητα και κάποιες φορές μπορεί να ματώνουν.Η υγιεινή του στόματος σας είναι πολύ σημαντική.','Το μωρό σας τώρα βάζει σταδιακά βάρος, και έχει μετατραπεί σε ένα μικροσκοπικό, γλιστερό πλασματάκι!  Μια κολλώδης λευκή ουσία αποκαλουμένη σμήγμα,  το οποίο εκκρίνουν οι αδένες που υπάρχουν στο δέρμα, καλύπτει ολόκληρο το σώμα του,  με σκοπό να το προστατέψει καθ’ όλη τη διάρκεια της μακρινής του παραμονής στο αμνιακό υγρό. Μερικά μωρά είναι ακόμη καλυμμένα με αυτή την ουσία όταν γεννιούνται.Μήκος περίπου 16-23 εκατοστά και βάρος 260-300 γραμμάρια.','https://www.youtube.com/watch?v=0bmH4q8PWnY');

INSERT INTO `gynecological_tests` (`id`, `week`,`test_title`,`test_details`, `yourself_info`,`yourbaby_info`,`video`) VALUES ('21', '21','Υπερηχογράφημα Β Επιπέδου','Ήρθε η στιγμή για το υπερηχογράφημα Β επιπέδου.Συμβουλευτείτε τον ιατρό σας.','Να προσέχετε τι λέτε από εδώ και πέρα γιατί πιθανότατα σας ακούει! Μπορείτε να επικοινωνείτε μαζί του μιλώντας του, τραγουδώντας του και γιατί όχι να του διαβάζετε φωναχτά; Μερικές έρευνες δείχνουν ότι ένα νεογέννητο τρώει καλύτερα την τροφή του όταν του διαβάζετε ένα βιβλίο που το είχε ήδη ακούσει ενώ ήταν ακόμη στην κοιλιά της μαμάς του.','Το παιδί σας είναι περίπου 23 εκατοστά από την κορυφή του κεφαλιού ως την φτέρνα. Τα φρύδια και τα βλέφαρα έχουν αναπτυχθεί πλήρως και τα νύχια του καλύπτουν την άκρη των δακτύλων. Επίσης το μωρό σας καταπίνει μέσα στην μήτρα αμνιακό υγρό, το οποίο  βοηθάει στην ανάπτυξη και εξέλιξη του πεπτικού συστήματος. Το αμνιακό υγρό που καταπίνει το μωρό σας εξαρτάται μεταξύ άλλων και από τις ανάγκες του σε θερμίδες.Μήκος περίπου 23-27 εκατοστά και βάρος 300-350 γραμμάρια.','https://www.youtube.com/watch?v=XFW3CBO-cvk');

INSERT INTO `gynecological_tests` (`id`, `week`,`test_title`,`test_details`, `yourself_info`,`yourbaby_info`,`video`) VALUES ('22', '22','Υπερηχογράφημα Β Επιπέδου','Ήρθε η στιγμή για το υπερηχογράφημα Β επιπέδου.Συμβουλευτείτε τον ιατρό σας.','Σε αυτό το σημείο, θα παρατηρήσετε πιθανότατα μια σταδιακή αύξηση του βάρους σας περίπου 225 γραμμάρια κάθε εβδομάδα. Επίσης θα παρατηρήσετε και μια άνοδο στην όρεξη σας. Δεν πειράζει να ικανοποιείτε περιστασιακά την όρεξη σας για παγωτό, αλλά προσπαθήστε να βρείτε μια υγιεινή ισορροπία εάν επιθυμείτε συνεχώς έτοιμα φαγητά.','Το μωρό σας τώρα είναι σαν μινιατούρα νεογέννητου. Τα χείλη του γίνονται πιο ευδιάκριτα και τα μάτια του έχουν αναπτυχθεί - αν και η ίριδα του ματιού χρειάζεται ακόμη χρώμα - τα φρύδια και οι βλεφαρίδες του είναι ήδη στην θέση τους. Το πάγκρεας (σημαντικό για την παραγωγή ορμονών), αναπτύσσεται σταδιακά και τα πρώτα σημάδια των δοντιών φαίνονται κάτω από τη γραμμή  των ούλων. Πριν να το καταλάβετε, το μωρό σας θα σας χαμογελάσει.Μήκος περίπου 24-28 εκατοστά και βάρος 350-450 γραμμάρια.','https://www.youtube.com/watch?v=XFW3CBO-cvk');

INSERT INTO `gynecological_tests` (`id`, `week`,`test_title`,`test_details`, `yourself_info`,`yourbaby_info`,`video`) VALUES ('23', '23','Υπερηχογράφημα Β Επιπέδου','Ήρθε η στιγμή για το υπερηχογράφημα Β επιπέδου.Συμβουλευτείτε τον ιατρό σας','Μπορεί να παρατηρήσετε κάποια υποτυπώδη αιμορραγία όταν βουρτσίζετε τα δόντια σας, ένα συνηθισμένο φαινόμενο της εγκυμοσύνης. Οι ορμόνες της  εγκυμοσύνης μπορεί να κάνουν τα ούλα σας να πρήζονται και να ερεθίζονται εύκολα που έχει ως αποτέλεσμα τη συχνή αιμορραγία, ειδικά όταν πλένετε τα δόντια σας.','Το μωρό σας είναι πιθανότατα γύρω στα 29 εκατοστά από την κορυφή του κεφαλιού μέχρι την φτέρνα. Τα αιμοφόρα αγγεία στους πνεύμονες αναπτύσσονται προετοιμάζοντας το αναπνευστικό σύστημα, ενώ πλέον καταπίνει κανονικά αν και φυσιολογικά δε θα «κάνει τα κακά του» μέχρι να γεννηθεί. Σε αυτό το στάδιο, έχει αρχίσει να αναπτύσσεται το πάγκρεας, το οποίο είναι σημαντικό για την παραγωγή ορμονών, ιδιαιτέρως της ινσουλίνης, που έχει καθοριστική σημασία για το μεταβολισμό.  Το κορμί του μωρού σας γίνεται τώρα πιο αφράτο, αν και το δερματάκι του παραμένει ρυτιδιασμένο καθώς θα πάρει κι άλλο βάρος στη συνέχεια. Το χνούδι του σώματος του σκουραίνει και το προσωπάκι καθώς και το σώμα του παίρνουν τώρα το τελικό τους σχήμα.Μήκος περίπου 24-29 εκατοστά και βάρος 450-500 γραμμάρια.','https://www.youtube.com/watch?v=XFW3CBO-cvk');

INSERT INTO `gynecological_tests` (`id`, `week`,`test_title`,`test_details`, `yourself_info`,`yourbaby_info`,`video`) VALUES ('24', '24','Υπερηχογράφημα Ανάπτυξης Doppler','Η επόμενη εξέταση που πρέπει να υποβληθείτε είναι το υπερηχογράφημα Ανάπτυξης (Doppler).Συμβουλευτείτε τον ιατρό σας.','Μπορεί να παρατηρήσετε ασθενείς κόκκινες γραμμές  στη κοιλιά σας, αλλιώς γνωστές ως ραγάδες, καθώς και στη μέση, τους γλουτούς και το στήθος σας. Πολλές γυναίκες  επίσης νιώθουν έντονη φαγούρα στο δέρμα τους, μιας και οι ορμόνες της εγκυμοσύνης μπορεί να κάνουν το δέρμα σας πιο ξηρό και η αύξηση της τάσης σε μια μεγάλη κοιλιά δεν βοηθάει. Η χρησιμοποίηση ειδικής κρέμας μπορεί να σας κάνει να αισθανθείτε πιο άνετα και να σας βοηθήσει να καταπραΰνετε την φαγούρα.','Το μωρό σας μεγαλώνει σταδιακά, βάζοντας περίπου 99 γραμμάρια την εβδομάδα. Το δέρμα του είναι λεπτό και εύθραυστο αλλά το σώμα του μεγαλώνει και πιάνει περισσότερο χώρο στην μήτρα σας. Μπορεί επίσης να αναπτύξει μια αδυναμία στα γλυκά. Οι προτιμήσεις στις γεύσεις διαμορφώνονται τώρα, και, πιστέψτε το ή όχι, η απόκτηση αδυναμίας στα γλυκά γίνεται τώρα.Μήκος περίπου 30 εκατοστά και βάρος 550-600 γραμμάρια.','https://www.youtube.com/watch?v=XFW3CBO-cvk ');

INSERT INTO `gynecological_tests` (`id`, `week`,`test_title`,`test_details`, `yourself_info`,`yourbaby_info`,`video`) VALUES ('25', '25','Υπερηχογράφημα Ανάπτυξης Doppler','Η επόμενη εξέταση που πρέπει να υποβληθείτε είναι το υπερηχογράφημα Ανάπτυξης (Doppler).Συμβουλευτείτε τον ιατρό σας.','Όσο περνάει ο καιρός και το έμβρυο μεγαλώνει τόσο αρχίζετε να νιώθετε πιο άβολα. Οι κράμπες στα πόδια γίνονται εντονότερες, οι πονοκέφαλοι κάποιες φορές είναι αφόρητοι και οι πόνοι στα πλευρά είναι ακόμη πιο αισθητοί.','Το μωρό σας αρχίζει να κάνει κάποιες αναπνευστικές κινήσεις, ωστόσο δεν υπάρχει αέρας στους πνεύμονες του ακόμη. Οι αισθήσεις του αυξάνονται γρήγορα.  Ειδικές εξετάσεις στον εγκέφαλο των εμβρύων δείχνουν ότι, από τις 26 εβδομάδες, τα μωρά μπορούν να αγγίξουν και αν ρίξετε φως στην κοιλιά σας, το μωρό σας θα γυρίσει το κεφάλι του, που όπως λένε οι ερευνητές δείχνει ότι τα οπτικά τους νεύρα λειτουργούν.Μήκος περίπου 30 εκατοστά και βάρος 700 γραμμάρια.','https://www.youtube.com/watch?v=XFW3CBO-cvk');

INSERT INTO `gynecological_tests` (`id`, `week`,`test_title`,`test_details`, `yourself_info`,`yourbaby_info`,`video`) VALUES ('26', '26','Υπερηχογράφημα Ανάπτυξης Doppler και καμπύλη σακχάρου','Οι επόμενες εξέτασεις που πρέπει να υποβληθείτε είναι το υπερηχογράφημα Ανάπτυξης (Doppler) και η καμπύλη σακχάρου.Συμβουλευτείτε τον ιατρό σας.','Το τρίτο και τελευταίο τρίμηνο κύησης ξεκίνησε. Αυτήν την περίοδο θα δείτε μια μικρή αύξηση στην πίεση του αίματος που είναι φυσιολιογική. Αν όμως το βάρος σας αυξηθεί ξαφνικά, αρχίζετε να βλέπετε θολά και τα άκρα σας πρηστούν υπερβολικά προσέξτε το ενδεχόμενο προεκλαμψίας. Στην 31 εβδομάδα θέτουμε το θέμα της προεκλαμψίας  πιο αναλυτικά. Σε ότι αφορά τη φυσική σας κατάσταση οι πόνοι στη μέση και στην πλάτη είναι πλεόν αρκετά ενοχλητικοί. Για να ανακουφιστείτε καθίστε κάτω και ανασηκώστε τα πόδια ψηλά.','Εάν μπορούσατε να δείτε το μωρό σας, θα βλέπατε την λάμψη από τα μπλε (ή καστανά, πράσινα, ανοιχτά καστανά) μάτια του, τα οποία τώρα αρχίζουν να ανοίγουν.  Επίσης, θα είναι σίγουρα ιδιαίτερα καθησυχαστικό για εσάς και τον σύντροφο σας να ακούτε τους ήχους της καρδιάς του, ξέροντας ότι λειτουργεί σωστά, κατά τις επισκέψεις σας στον γυναικολόγο σας.  Η ανταπόκριση του μωρού σας στους ήχους αυξάνεται πιο σταθερά μέχρι το τέλος του έκτου μήνα, όταν ο σχηματισμός των ακουστικών νεύρων  θα έχει ολοκληρωθεί.Μήκος περίπου 32-36 εκατοστά και βάρος 880 γραμμάρια.','https://www.youtube.com/watch?v=XFW3CBO-cvk');

INSERT INTO `gynecological_tests` (`id`, `week`,`test_title`,`test_details`, `yourself_info`,`yourbaby_info`,`video`) VALUES ('27', '27','Υπερηχογράφημα Ανάπτυξης Doppler και καμπύλη σακχάρου','Οι επόμενες εξέτασεις που πρέπει να υποβληθείτε είναι το υπερηχογράφημα Ανάπτυξης (Doppler) και η καμπύλη σακχάρου.Συμβουλευτείτε τον ιατρό σας.','Τώρα πια το σώμα σας μεταβάλλεται ριζικά: η μήτρα σας είναι κοντά στα πλευρά σας  και εάν είστε άτυχη θα βιώνετε τις κράμπες στα πόδια, τις αιμορροΐδες  ή τους κιρσούς στα πόδια. (Εάν σας παρηγορεί, να ξέρετε ότι σχεδόν θα εξαφανιστούν μετά την γέννα).','Τώρα που πλησιάζετε το τελικό στάδιο της εγκυμοσύνης το μωρό σας πραγματικά αρχίζει να μεγαλώνει και να γεμίζει τον κενό χώρο της μήτρας. Τώρα μπορεί να ανοίξει και να κλείσει τα μάτια του, να κοιμηθεί και να ξυπνήσει συχνά, και πιθανόν να ρουφήξει τα δάχτυλά του όπως τον αντίχειρα. Αν και είναι ακόμη ανώριμοι, οι πνεύμονες του μωρού σας θα λειτουργήσουν με λίγη ιατρική βοήθεια, εάν γεννηθεί πρόωρα. Τώρα κινείται περισσότερο, καθησυχάζοντας την μητέρα, καθιστώντας το μια ευχάριστη εμπειρία και για τον μέλλοντα πατέρα.Μέγεθος περίπου 34-37 εκατοστά και βάρος 990 γραμμάρια - 1 κιλό.','https://www.youtube.com/watch?v=XFW3CBO-cvk');

INSERT INTO `gynecological_tests` (`id`, `week`,`test_title`,`test_details`, `yourself_info`,`yourbaby_info`,`video`) VALUES ('28', '28','Υπερηχογράφημα Ανάπτυξης Doppler και καμπύλη σακχάρου','Οι επόμενες εξέτασεις που πρέπει να υποβληθείτε είναι το υπερηχογράφημα Ανάπτυξης (Doppler) και η καμπύλη σακχάρου.Συμβουλευτείτε τον ιατρό σας.','Από αυτήν την εβδομάδα οι επισκέψεις στον γυναικολόγο σας γίνονται πιο τακτικές. Από μια φορά το μήνα θα πηγαίνετε πλεόν για  τον καθιερωμένο προγεννητικό έλεγχο στον γιατρό σας  κάθε δυο εβδομάδες και αυτό θα συνεχιστεί μέχρι την 36 εβδομάδα. Από εκεί και έπειτα και μέχρι να γεννήσετε, θα επισκέπτεστε τον γιατρό σας κάθε εβδομάδα.','Το μωρό σας μπορεί τώρα πια να ανοίξει τα μάτια του και να γυρίσει το κεφάλι του σε οποίο σημείο φανεί φως. Τα νύχια των χεριών του πρωτοεμφανίζονται και στρώσεις λίπους έχουν αρχίσει να σχηματίζονται καθώς προετοιμάζεται να βγει στον έξω κόσμο. Σε αυτό το στάδιο, ο εγκέφαλος του μωρού σας αρχίζει να σχηματίζει στην επιφάνεια του τις χαρακτηριστικές έλικες και αύλακες και αυξάνει την ποσότητα του εγκεφαλικού ιστού.  Εμφανίζονται τα φρύδια και οι βλεφαρίδες του και τα μαλλάκια του αρχίζουν να μακραίνουν.Μέγεθος περίπου 35-38 εκατοστά και βάρος 1.100 γραμμάρια.','https://www.youtube.com/watch?v=ARbLr7ZOZpY');

INSERT INTO `gynecological_tests` (`id`, `week`,`test_title`,`test_details`, `yourself_info`,`yourbaby_info`,`video`) VALUES ('29', '29','Υπερηχογράφημα Ανάπτυξης Doppler και καμπύλη σακχάρου','Ήρθε η στιγμή που πρέπει να κάνετε το Υπερηχογράφημα Ανάπτυξης Doppler.Συμβουλευτείτε τον ιατρό σας.','Τα άκρα σας έχουν πρηστεί αρκετά με αποτέλεσμα τα παπούτσια σας να σας στενεύουν και το δαχτυλίδι σας να μην μπορεί να βγει εύκολα από το δάχτυλο σας. Τις επόμενες εβδομάδες το στήθος σας θα πρηστεί ακόμη περισσότερο και πιθανότατα θα  αλλάξετε το νούμερο του σουτιέν σας. Οι κράμπες σας ταλαιπωρούν αρκετά ειδικά τη νύχτα, οπότε τα συχνά ποδόλουτρα θα σας ανακουφίσουν.','Ο εγκέφαλος του μωρού σας αναπτύσσεται σταδιακά και το κεφάλι του πλαταίνει για να μπορέσει να ανταπεξέλθει στο μέγεθος αυτό. Εάν πρόκειται να αποκτήσετε αγόρι, οι όρχεις του μετακινούνται από τη αρχική τους θέση που είναι κοντά στα νεφρά προς τη βουβωνική χώρα. Εάν πρόκειται να αποκτήσετε κορίτσι, η κλειτορίδα προεξέχει ακόμα επειδή τα μικρά χείλη του αιδοίου δεν την καλύπτουν ακόμη. Αυτό θα συμβεί την τελευταία εβδομάδα πριν από την γέννηση.Καθώς η ανάπτυξη του μωρού σας είναι ταχύτατη και ενώ τα μωρά διαφέρουν μεταξύ τους ως προς το βάρος και το μήκος, είναι αξιοσημείωτο ότι τα πρόωρα μωρά είναι πιο μικρόσωμα. Έστω και λίγες εβδομάδες μέσα στην μήτρα, συντείνουν στην ανάπτυξη του σημαντικά (ακούτε εσείς ανυπόμονες μελλοντικές μαμάδες που δεν αντέχετε και θέλετε να βγει το μωράκι σας όσο το δυνατόν ταχύτερα;).  Δύο ενδιαφέροντες παράγοντες αναφορικά με το βάρος του μωρού:(1)τα αγόρια είναι βαρύτερα από τα κορίτσια και (2)το βάρος του μωρού σας αυξάνει ανάλογα με τον αριθμό κυήσεων της μητέρας.Μέγεθος περίπου 37-40 εκατοστά και βάρος 1300 γραμμάρια.','https://www.youtube.com/watch?v=ARbLr7ZOZpY');

INSERT INTO `gynecological_tests` (`id`, `week`,`test_title`,`test_details`, `yourself_info`,`yourbaby_info`,`video`) VALUES ('30', '30','Υπερηχογράφημα Ανάπτυξης Doppler και καμπύλη σακχάρου','Ήρθε η στιγμή που πρέπει να κάνετε το Υπερηχογράφημα Ανάπτυξης Doppler.Συμβουλευτείτε τον ιατρό σας.','Τώρα θα πρέπει να γίνει ο τρίτος (αν ενδιάμεσα δεν έγιναν άλλοι) υπερηχογραφικός έλεγχος. Θα μας δώσει πληροφορίες για την ανάπτυξη του παιδιού και ενδεχομένως για την κατάσταση του πλακούντα. Οι αναπνευστικές κινήσεις του παιδιού είναι συχνότερες. Με αυτές εξασκείται το μυϊκό σύστημα και αποτελούν μία παράμετρο καλής κατάστασης του παιδιού. Οι αναπνευστικές κινήσεις αρχίζουν από την 20η εβδομάδα.','Έχει την πλήρη δυνατότητα της γεύσης. Το μήκος του είναι 27 εκ και ζυγίζει 1,5 κιλό. Η διάμετρος της κεφαλής (Η αμφιβρεγματική) είναι σχεδόν 8 εκ. και οι πατούσες του έχουν μήκος 6 εκ.Το ζαρωμένο δέρμα αρχίζει να τεντώνεται. Στα αγοράκια οι όρχεις βρίσκονται στην βουβωνική χώρα (μέσα στο τοίχωμα). Το μωρό σας είναι τώρα σε θέση να ρυθμίσει μόνο του την θερμοκρασία του.Μέγεθος περίπου 40-42 εκατοστά και βάρος 1400-1500 γραμμάρια.','https://www.youtube.com/watch?v=ARbLr7ZOZpY');

INSERT INTO `gynecological_tests` (`id`, `week`,`test_title`,`test_details`, `yourself_info`,`yourbaby_info`,`video`) VALUES ('31', '31','Υπερηχογράφημα Ανάπτυξης Doppler και καμπύλη σακχάρου','Ήρθε η στιγμή που πρέπει να κάνετε το Υπερηχογράφημα Ανάπτυξης Doppler.Συμβουλευτείτε τον ιατρό σας.','Παρατηρείστε το σώμα σας ιδιαίτερα προσεκτικά για τυχόν σημάδια προεκλαμψίας. Αν το βάρος σας αυξηθεί ξαφνικά, αρχίζετε να βλέπετε θολά και τα άκρα σας πρηστούν υπερβολικά  εοικοινωνήστε αμέσως με το γιατρό σας. Η προεκλαμψία είναι η συνηθέστερη σοβαρή επιπλοκή της κύησης. Τις περισσότερες φορές είναι ήπιας μορφής ωστόσο κάποιες φορές μπορεί να αποδειχθεί εξαιρετικά επικίνδυνη τόσο για την μητέρα όσο και για το έμβρυο.  μια στις δέκα γυναίκες παρουσιάζει κάποια ευρύτερη μορφή προεκλαμψίας. Μεγαλύτερο κίνδυνο διατρέχουν οι γυναίκες που κυοφορούν για πρώτη φορά, οι γυναίκες άνω των 40, όσες παρουσιάζουν δείκτη σωματικής μάζας υψηλότερο του 35, οι γυναίκες με οικογενειακό ιστορικό προεκλαμψίας, όσες απέκτησαν το προηγούμενο παιδί τους πριν από 10 χρόνια, οι πάσχουσες από υπέρταση, διαβήτη ή κάποια νεφρική πάθηση. Το αίτιο που την προκαλεί είναι ένα πρόβλημα στον πλακούντα που περιορίζει τη ροή αίματος προς το έμβρυο. Το πρόβλημα αυτό αναπτύσσεται στην αρχή της κύησης, αλλά η πάθηση εκδηλώνεται πολύ αργότερα – σνήθως τις τελευταίες εβδομάδες. Υψηλή αρτηριακή πίεση, πρωτεϊνουρία στη μητέρα και, μερικές φορές, υπανάπτυξη του εμβρύου είναι τα βασικά συμπτώματα τα οποία μπορούν να εντοπισθούν από τους καθιερωμένους προγεννητικούς ελέγχους.Παρατηρείστε το σώμα σας ιδιαίτερα προσεκτικά για τυχόν σημάδια προεκλαμψίας. Αν το βάρος σας αυξηθεί ξαφνικά, αρχίζετε να βλέπετε θολά και τα άκρα σας πρηστούν υπερβολικά  εοικοινωνήστε αμέσως με το γιατρό σας. Η προεκλαμψία είναι η συνηθέστερη σοβαρή επιπλοκή της κύησης. Τις περισσότερες φορές είναι ήπιας μορφής ωστόσο κάποιες φορές μπορεί να αποδειχθεί εξαιρετικά επικίνδυνη τόσο για την μητέρα όσο και για το έμβρυο.  μια στις δέκα γυναίκες παρουσιάζει κάποια ευρύτερη μορφή προεκλαμψίας. Μεγαλύτερο κίνδυνο διατρέχουν οι γυναίκες που κυοφορούν για πρώτη φορά, οι γυναίκες άνω των 40, όσες παρουσιάζουν δείκτη σωματικής μάζας υψηλότερο του 35, οι γυναίκες με οικογενειακό ιστορικό προεκλαμψίας, όσες απέκτησαν το προηγούμενο παιδί τους πριν από 10 χρόνια, οι πάσχουσες από υπέρταση, διαβήτη ή κάποια νεφρική πάθηση. Το αίτιο που την προκαλεί είναι ένα πρόβλημα στον πλακούντα που περιορίζει τη ροή αίματος προς το έμβρυο. Το πρόβλημα αυτό αναπτύσσεται στην αρχή της κύησης, αλλά η πάθηση εκδηλώνεται πολύ αργότερα – συνήθως τις τελευταίες εβδομάδες. Υψηλή αρτηριακή πίεση, πρωτεϊνουρία στη μητέρα και, μερικές φορές, υπανάπτυξη του εμβρύου είναι τα βασικά συμπτώματα τα οποία μπορούν να εντοπισθούν από τους καθιερωμένους προγεννητικούς ελέγχους.','Αυτήν την εβδομάδα οι σύνδεσμοι του εγκεφάλου του μωρού σας αναπτύσσονται με εντυπωσιακό ρυθμό. Τα μάτια του ανοίγουν τελείως και μπορεί να διακρίνει σκοτάδι και φως. Το μωρό σας αφιερώνει τώρα περισσότερη ώρα για ύπνο, κυρίως ύπνο rem, γι αυτό πιθανώς έχετε παρατηρήσει πιο συγκεκριμένη μορφή ωρών εγρήγορσης και ύπνου του μικρού σας. Τα περισσότερα παιδιά έχουν κάνει στροφή με το κεφάλι προς τα κάτω. Βεβαίως η θέση αυτή μπορεί ακόμη στην πορεία να αλλάξει.Μήκος περίπου 40-42 εκατοστά και βάρος 1500-1600 γραμμάρια.','https://www.youtube.com/watch?v=ARbLr7ZOZpY');

INSERT INTO `gynecological_tests` (`id`, `week`,`test_title`,`test_details`, `yourself_info`,`yourbaby_info`,`video`) VALUES ('32', '32','Υπερηχογράφημα Ανάπτυξης Doppler και καμπύλη σακχάρου','Ήρθε η στιγμή που πρέπει να κάνετε το Υπερηχογράφημα Ανάπτυξης Doppler.Συμβουλευτείτε τον ιατρό σας.','Θα νιώθετε το μωρό σας να στριφογυρίζει και να σας κλωστάει πολύ συχνά. Οι πόνοι στην πλάτη όσο παίρνετε βάρος κάνουν την εμφάνιση τους και εσείς μπορείτε να ανκουφιστείτε κάνοντας κάποιες ασκήσεις στο σπίτι ή σε κάποιον άλλον κατάλληλα διαμορφωμένο χώρο.','Το μωρό σας μπορεί να χρησιμοποιεί τώρα και τις πέντε του αισθήσεις. Νιώθει πλέον σχεδόν όλον τον χώρο μέσα στη μήτρα και εξασκείται στο να ανοιγοκλείνει τα μάτια και να κάνει αναπνευστικές κινήσεις. Επίσης μπορεί να πιπιλίζει τον αντίχειρα του για λίγο. Με τη μεγαλύτερη  συσσώρευση  λίπους  κάτω από το δέρμα, πλέον το δέρμα δεν είναι διάφανο αλλά πιο αδιαφανές. Οι κινήσεις του αυξάνονται όσο έχει ακόμη χώρο μέσα στη μήτρα και εσείς το νιώθετε ακόμη περισσότερο.Μήκος περίπου 41-43 εκατοστά και βάρος 1800 γραμμάρια.','https://www.youtube.com/watch?v=ARbLr7ZOZpY');

INSERT INTO `gynecological_tests` (`id`, `week`,`test_title`,`test_details`, `yourself_info`,`yourbaby_info`,`video`) VALUES ('33', '33','Υπερηχογράφημα 3o Τριμήνου','Έχετε κάνει το Υπερηχογράφημα του 3ου Τριμήνου.Συμβουλευτείτε τον ιατρό σας.','Το βάρος σας αυξάνεται αισθητά ίσως περισσότερο από κάθε άλλη φορά στη διάρκεια της εγκυμοσύνης. Αυτό ασφαλώς δικαιολογείται και από τη ραγδαία ανάπτυξη του μωρού σας.  Μερικές έγκυες διαμαρτύρονται για μία σχετική αδράνεια του εντέρου (δυσκοιλιότητα). Φροντίστε τη διατροφή  σας, πιείτε αρκετά υγρά, πάρτε μαγνήσιο προκειμένου να διευκολύνετε τη λειτουργία του εντέρου σας.','Το μωρό σας θα συνεχίζει να κερδίζει αρκετό βάρος τις τελευταίες εβδομάδες μέχρι τη γέννηση του. Το λίπος κάτω από το δέρμα αλλάζει το χρώμα του δέρματος του μωρού σας από κόκκινο σε ροζ. Ο εγκέφαλος του μωρού σας συνεχίζει να αναπτύσσεται και να ωριμάζει . Το επίπεδο του αμνιακού υγού σας έχει φτάσει στο μέγιστο λόγω του μεγέθους του εμβρύου, γι αυτό και οι κλωτσιές και τα χτυπήματα που νιώθετε είναι ακόμη πιο δυνατά και πολλές φορές δυσάρεστα. Επίσης σε αυτήν τη φάση περνούν αντισώματα από εσάς στο μωρό σας καθώς εκείνο συνεχίζει να αναπτύσσει το δικό τους ανοσοποιητικό σύστημα.Μήκος περίπου 44 εκατοστά και βάρος 2 κιλά.','https://www.youtube.com/watch?v=ARbLr7ZOZpY');

INSERT INTO `gynecological_tests` (`id`, `week`,`test_title`,`test_details`, `yourself_info`,`yourbaby_info`,`video`) VALUES ('34', '34','Υπερηχογράφημα 3o Τριμήνου','Έχετε κάνει το Υπερηχογράφημα του 3ου Τριμήνου.Συμβουλευτείτε τον ιατρό σας.','Αν και το έμβρυο συνεχίζει να αυξάνει το βάρος του, εσείς αρχίζετε σταδιακά  να μην παίρνετε πολύ βάρος. Αυτό κυρίως οφείλεται στο γεγονός ότι οι καούρες αυξάνονται με αποτέλεσμα τα γεύματα σας να μην είναι το ίδιο απολαυστικά. Φορέστε ρούχα που δεν πιέζουν το στήθος και το στομάχι σας για να νιώθετε πιο άνετα. Οι εξετάσεις θα πρέπει τώρα να προγραμματίζονται συχνότερα. Διαβάστε περισσότερες λεπτομέρειες στην κατηγορία προγεννητικός έλεγχος.','Αυτήν την εβδομάδα τα νύχια του μωρού σας έχουν αναπτυχθεί πλήρως και πιθανώς έχουν φτάσει στις άκρες των δαχτύλων. To μωρό σας ουρεί σχεδόν μια πίντα την ημέρα. Στην πραγματικότητα τα ούρα μαζί και με τα άλλα υγρά συμβάλλουν στο αμνιακό υγρό το οποίο ανανεώνεται από μόνο του κάθε τρεις ώρες. Αν  έχετε αγόρι αρχίζουν να κατεβαίνουν οι όρχεις από τη κοιλιά στη σωστή τους θέση.  Σε περίπτωση προώρου τοκετού έχει θεωρητικά όλες τις προϋποθέσεις να επιβιώσει χωρίς σημαντικές επιπλοκές. Παιδιά που γεννιούνται νωρίτερα, έχουν χάρη στην μοντέρνα τεχνολογία επίσης καλές πιθανότητες επιβίωσης.Το έμβρυο έχει τώρα το μέγεθος ενός ανανά.Μήκος περίπου 45 εκατοστά και βάρος 2.200 κιλά.','https://www.youtube.com/watch?v=ARbLr7ZOZpY');

INSERT INTO `gynecological_tests` (`id`, `week`,`test_title`,`test_details`, `yourself_info`,`yourbaby_info`,`video`) VALUES ('35', '35','Υπερηχογράφημα 3o Τριμήνου','Έχετε κάνει το Υπερηχογράφημα του 3ου Τριμήνου.Συμβουλευτείτε τον ιατρό σας.','Ο ύπνος σας τα βράδια μπορεί να σας δυσκολεύει ακόμη περισσότερο από αυτήν την εβδομάδα και μέχρι να γεννήσετε. Μπορεί να ξυπνάτε πολύ συχνά κατά τη διάρκεια της νύχτας και να επισκέπτεστε το μπάνιο σας ή να νιώθετε άβολα με οποιαδήποτε στάση στο κρεββάτι σας! Υπομονή! Χρησιμοποιήστε αρκετά μαξιλάρια  προκειμένου να στηρίξετε τη μέση, την πλάτη και τα πόδια σας.','Η ανάπτυξη του εγκεφάλου του εμβρύου σας αναπτύσσεται με γοργό ρυθμό. Το κεφάλι του μπορεί να είναι μεγάλο αλλά είναι ακόμα μαλακό ώστε να περάσει από τον στενό γεννητικό σωλήνα. Το μωρό σας συνεχίζει να κερδίζει βάρος  ενώ η ανάπτυξη ως προς το ύψος μειώνεται. Επίσης κερδίζει και αρκετό λίπος που θα το κρατήσει ζεστό και ασφαλές όταν θα βγει στον έξω κόσμο. Αν γεννηθεί πρόωρα πιθανότατα να καταφέρει να θηλάσει με κάποια ωστόσο προβλήματα.Μήκος περίπου 46 εκατοστά και βάρος περίπου 2 κιλά και 430 γραμμάρια.','https://www.youtube.com/watch?v=ARbLr7ZOZpY');

INSERT INTO `gynecological_tests` (`id`, `week`,`test_title`,`test_details`, `yourself_info`,`yourbaby_info`,`video`) VALUES ('36', '36','Υπερηχογράφημα 3o Τριμήνου','Έχετε κάνει το Υπερηχογράφημα του 3ου Τριμήνου.Συμβουλευτείτε τον ιατρό σας.','Είναι σημαντικό να "ακούτε¨το μωρό σας αρκετές φορές την ημέρα μέχρι το τέλος της εγκυμοσύνης. Σε περίπτωση που δεν το νιώσετε για αρκετές ώρες επικοινωνήστε με το γιατρό σας ο οποίος θα σας πληροφορήσει σχετικά. Αρχίζετε επίσης τώρα να αντιλαμβάνεστε τις συσπάσεις  της  μήτρας περισσότερο. Αυτές είναι προπαρασκευαστικές συσπάσεις και κατά κανόνα δεν είναι επώδυνες.','Ο χώρος μέσα στην μήτρα όσο περνάει ο καιρός γίνεται και στενότερος.  Το υγρό που περιέχεται στη μήτρα γίνεται λιγότερο και οι κινήσεις του εμβρύου αραιώνουν. Τα περισσότερα από τα συστήματα του μωρού είναι σχεδόν εξοπλισμένα για τη ζωή έξω από τη μήτρα. Η ανάπτυξη του πεπτικού σύστηματος έχει ολοκληρωθεί και θα τεθεί σε λειτουργία μόλις το μωρό αρχίζει να πιπιλίζει για πρώτη φορά το στήθπς σας. Άλλωστε μέχρι τη γέννηση του, η διατροφή του εμβρύου γίνεται μέσω του ομφάλιου λώρου και δε χρειάζεται πέψη.Μήκος περίπου 47 εκατοστά και βάρος 2650 γραμμάρια.','https://www.youtube.com/watch?v=ARbLr7ZOZpY');

INSERT INTO `gynecological_tests` (`id`, `week`,`test_title`,`test_details`, `yourself_info`,`yourbaby_info`,`video`) VALUES ('37', '37','Υπερηχογράφημα 3o Τριμήνου','Έχετε κάνει το Υπερηχογράφημα του 3ου Τριμήνου.Συμβουλευτείτε τον ιατρό σας.','Ο γιατρός σας, σας παρακολουθεί τώρα πιο στενά και σε κάθε επίσκεψη ελέγχει την ανάπτυξη και την θέση του μωρού σας.  Τώρα μπορεί να προγραμματιστεί και ένας προγεννητικός καρδιοτοκογραφικός έλεγχος. Δεν αποτελεί βέβαια αναγκαστικά ένδειξη σαν εξέταση ρουτίνας. Για το θέμα του προγεννητικού ελέγχου μπορείτε να ενημερώνεστε από τη σχετική  μας ενότητα.','Το μωρό σας εξασκείται κάνοντας αναπνευστικές κινήσεις προκειμένου να ανταπεξέλθει στη ζωή έξω από τη μήτρα. Πιπιλίζει τον αντίχειρα του, ανοιγοκλείνει τα μάτια του και γυρίζει από τη μια πλευρά στην άλλη.  Το μέσο έμβρυο αυτής της ηλικίας εξακολουθεί να παίρνει βάρος με ρυθμό μισού κιλού την εβδομάδα περίπου. Τα μωρά ανάμεσα από 37 έως την 42 εβδομάδα θεωρούνται τελειόμηνα. Αν ένα μωρό γεννηθεί πριν από την 37 εβδομάδα θεωρείται πρόωρο. Το έμβρυο έχει τώρα το μέγεθος ενός πεπονιού.Μήκος περίπου 49 εκατοστά και βάρος περίπου 2 κιλά και 870 γραμμάρια.','https://www.youtube.com/watch?v=ARbLr7ZOZpY');

INSERT INTO `gynecological_tests` (`id`, `week`,`test_title`,`test_details`, `yourself_info`,`yourbaby_info`,`video`) VALUES ('38', '38','Υπερηχογράφημα 3o Τριμήνου','Έχετε κάνει το Υπερηχογράφημα του 3ου Τριμήνου.Συμβουλευτείτε τον ιατρό σας.','Η αντίστροφη μέτρηση έχει ξεκινήσει. Καλό είναι να έχετε ετοιμάσει τη βαλίτσα του μαιευτηρίου γιατί από στιγμή σε στιγμή  το μωράκι σας μπορεί να έρθει στον κόσμο. Οι πιθανές καούρες που νιώθετε τις τελευταίες εβδομάδες συνεχίζονται και εσείς πρέπει απλά να κάνετε λίγο ακόμη υπομονή.','Το μωρό σας καταπίνει πολύ αμνιακό υγρό. Tα πρώτα κόπρανα, το λεγόμενο μηκώνιο συσσωρεύεται στα έντερα. Το μωρό ρίχενι το εμβρυονικό σμήγμα και το χνούδι που προστατεύουν το δέρμα του. Επίσης παράγει περισσότερο επιφανειοδραστικό παράγοντα που θα αποτρέψει τους σάκους αέρα στους πνεύμονες του να κολλήσουνμεταξύ τους όταν αρχίζει να αναπνέει.Μήκος περίπου 50 εκατοστά και βάρος περίπου 3 κιλά και 30 γραμμάρια.',null);

INSERT INTO `gynecological_tests` (`id`, `week`,`test_title`,`test_details`, `yourself_info`,`yourbaby_info`,`video`) VALUES ('39', '39','Υπερηχογράφημα 3o Τριμήνου','Έχετε κάνει το Υπερηχογράφημα του 3ου Τριμήνου.Συμβουλευτείτε τον ιατρό σας.','Η αλλαγή στη θέση του μωρού μπορεί να διευκολύνει την αναπνοή σας αλλά μπορεί να σας δυσκολέψει στις κινήσεις και το περπάτημά σας. Οι πόνοι στη ν πλάτη πιθανόν να είναι πιο έντονοι λόγω του βάρους που έχετε πάρει. Προσπαθήσεστε να φοράτε όσο πιο άνετα ρούχα και παπούτσια μπορείτε για να νιώθετε πιο άνετα. Οι πολύ λεπτές γυναίκες μπορούν να πάρουν περισσότερα κιλά, ενώ οι πιο σωματώδεις είναι προτιμότερο να βάλουν λιγότερα.','Το μωρό σας με δυσκολία καταφέρνει να κινείται μέσα στη μήτρα . Οι πνεύμονες έχουν ωριμάσει  και ο εγκέφαλος μεγαλώνει και αναπτύσσεται με ταχύ ρυθμό που θα συνεχιστεί κατά τη διάρκεια των 3 πρώτων χρόνων της ζωής του. Το ροζ δέρμα του μωρού σας έχει γίνει άσπρο ή ασπριδερό. Αυτήν την εβδομάδα είναι ότι πιθανόν το κεφάλι του μωρού σας  να έχει εμπεδωθεί στην πύελο, αν το μωρό έχει "γυρίσει", γεγονός που θα σας επιτρέψει να γεννήσετε πιο εύκολα.Μήκος περίπου 48,5-50 εκατοστά και βάρος 3.100-3.200 γραμμάρια.',null);

INSERT INTO `gynecological_tests` (`id`, `week`,`test_title`,`test_details`, `yourself_info`,`yourbaby_info`,`video`) VALUES ('40', '40','Δεν υπάρχει κάποια εξέταση','Οι εξετάσεις πλέον έχουν σταματήσει ήρθε η στιγμή που τόσο περιμένατε.',null,'Φτάσατε στο τέλος της εγκυμοσύνης σας και η πιθανή ημερομηνία τοκετού έφτασε.Το μωρό σας είναι απολύτως τελειόμηνο και το βάρος του μπορεί να είναι από 3 έως 4,5 κιλά όπως και το ύψος του από 48,5 έως 55 εκατοστά. Η διάμετρος της κεφαλής είναι περίπου 9.5 εκατοστά. Εμείς ενδεικτικά σας αναφέρουμε κάποιες τιμές. Σε ελάχιστες περιπτώσεις τα μωρά γεννιούνται στην υπολογισμένη ημερομηνία τοκετού. Οι μισές σχεδόν κυήσεις ξεπερνούν το όριο των 40 εβδομάδων αλλά όχι το όριο των 42 εβδομάδων. Από περίπτωση σε περίπτωση ο γυναικολόγος θα κρίνει πόσες μέρες θα περιμένει και πότε ενδεχομένως θα προγραμματίσει έναν τοκετό με πρόκληση. Η μεγάλη ώρα έφτασε! Καλή σας επιτυχία!Μέγεθος μωρού 50 εκατοστά και βάρος περίπου 3280 γραμμάρια.',null);



