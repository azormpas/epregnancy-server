package com.iBabyApp;


import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import com.iBabyApp.dto.ServiceError;
import com.iBabyApp.utils.ServiceException;

	@ControllerAdvice
	public class ServiceExceptionMapper extends ResponseEntityExceptionHandler {
	 
	    @ExceptionHandler(value = {Exception.class})
	    protected ResponseEntity<ServiceError> handleConflict(Exception ex, WebRequest request) {
	    	 	   
		    	 ServiceException se = ServiceException.wrap(ex);	
		    	 ServiceError error = new ServiceError(se.getErrorCode());	
		    	 return new ResponseEntity<ServiceError>(error,HttpStatus.CONFLICT);
	    }
	}

	
