package com.iBabyApp;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.Bean;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;





@SpringBootApplication
public class IBabyAppApplication extends SpringBootServletInitializer {
	
	public final static Logger logger = LoggerFactory.getLogger(IBabyAppApplication.class);

	
	public static void main(String[] args) {
		SpringApplication.run(IBabyAppApplication.class, args);
	}
	
	@Bean
	public PasswordEncoder passwordEncoder() {
		return new BCryptPasswordEncoder(12);
	}
	

	
}
