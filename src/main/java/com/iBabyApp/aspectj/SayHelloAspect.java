package com.iBabyApp.aspectj;


import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;
import org.springframework.beans.factory.annotation.Autowired;

@Component
@Aspect
public class SayHelloAspect {
	
	
	//@Autowired
	//@Pointcut("execution(@com.iBabyApp.annotation.SayHello * *(..))")
	public void sayHelloAnnotation() {}
	
	

	//@Around("sayHelloAnnotation()")
	public Object wrapTransaction(ProceedingJoinPoint joinPoint) throws Throwable {
	
		Object o = null;

		//System.out.println("HELLO FROM ASPECTJ BEFORE ACTUAL CODE !!!!!!!!!!!!!!!!!!!!!!!!!!!");
		//Run the actual code
		o = joinPoint.proceed();

		//System.out.println("HELLO FROM ASPECTJ AFTER ACTUAL CODE !!!!!!!!!!!!!!!!!!!!!!!!!!!");

		return o;

		}
}
		
