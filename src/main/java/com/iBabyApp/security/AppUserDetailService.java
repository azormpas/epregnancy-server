package com.iBabyApp.security;

import java.util.Collections;

import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;

import com.iBabyApp.dao.UserDAO;
import com.iBabyApp.model.User;

@Component
public class AppUserDetailService implements UserDetailsService {

	private final UserDAO userDAO;

	public AppUserDetailService(UserDAO userDAO) {
		this.userDAO = userDAO;
	}

	@Override
	public final UserDetails loadUserByUsername(String email)
			throws UsernameNotFoundException {
		
		
		final User user = (User) this.userDAO.findUser(email);
		
		
		if (user == null) {
			throw new UsernameNotFoundException("User '" + email + "' not found");
		}

	
		 
		 

		return org.springframework.security.core.userdetails.User
			.withUsername(email)
			.password(user.getPassword())
			.authorities(Collections.emptyList())
			.accountExpired(false)
			.accountLocked(false)
			.credentialsExpired(false)
			.disabled(false)
			.build();
	}
	
	


}