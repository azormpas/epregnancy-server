package com.iBabyApp.model;

import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;


@Entity
@Table(name="forum_answer")
public class ForumAnswer {
	
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="id")
    private long id;  
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="post_id", insertable = true, updatable = false)
    private ForumPost forumPost;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="user_id", insertable = true, updatable = false)
	private User user;
	
    @OneToMany(mappedBy = "forumAnswer", orphanRemoval = true)
    private List<ForumAnswerImage> forumAnswerImages;
	
	@Column(name="content")	
	private String content;
	
	/**
	 * The status describes if the post has been deleted or not. 
	 * The 0 value means that is active and the 1 value means that has been deleted.
	 */
	@Column(name="status")
	private boolean status;

    @Column(name = "created_at", columnDefinition = "DATETIME")
    @Temporal(TemporalType.TIMESTAMP)
	private Date createdAt;
	
    @Column(name = "updated_at", columnDefinition = "DATETIME")
    @Temporal(TemporalType.TIMESTAMP)
	private Date updatedAt;
    
	public ForumAnswer() {
		super();
		// TODO Auto-generated constructor stub
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public ForumPost getForumPost() {
		return forumPost;
	}

	public void setForumPost(ForumPost forumPost) {
		this.forumPost = forumPost;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public boolean isStatus() {
		return status;
	}

	public void setStatus(boolean status) {
		this.status = status;
	}

	public Date getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}

	public Date getUpdatedAt() {
		return updatedAt;
	}

	public void setUpdatedAt(Date updatedAt) {
		this.updatedAt = updatedAt;
	}

	public List<ForumAnswerImage> getForumAnswerImages() {
		return forumAnswerImages;
	}

	public void setForumAnswerImages(List<ForumAnswerImage> forumAnswerImages) {
		this.forumAnswerImages = forumAnswerImages;
	}
	
	

}
