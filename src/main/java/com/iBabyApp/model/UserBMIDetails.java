package com.iBabyApp.model;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
@Table(name="user_bmi")
public class UserBMIDetails {

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="id")
    private Long id;  


	@Column(name="twins")
    private boolean twins;
    

    @Column(name="current_weight")
    private Double currentWeight;  
    
    @Column(name="weight_before")
    private Double weightBefore;  
    
    @Column(name="week")
    private Integer week; 
    
    @Column(name="height")
    private Double height; 
    
    @Column(name="bmi")
    private Double bmi;
    
    
    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id",updatable = false)
    private User user; 
    


	public UserBMIDetails() {
		super();
		// TODO Auto-generated constructor stub
	}



	public Long getId() {
		return id;
	}



	public void setId(Long id) {
		this.id = id;
	}



	public boolean isTwins() {
		return twins;
	}



	public void setTwins(boolean twins) {
		this.twins = twins;
	}




	public Double getCurrentWeight() {
		return currentWeight;
	}



	public void setCurrentWeight(Double currentWeight) {
		this.currentWeight = currentWeight;
	}



	public Double getWeightBefore() {
		return weightBefore;
	}



	public void setWeightBefore(Double weightBefore) {
		this.weightBefore = weightBefore;
	}



	public Double getHeight() {
		return height;
	}



	public void setHeight(Double height) {
		this.height = height;
	}



	public Double getBmi() {
		return bmi;
	}



	public void setBmi(Double bmi) {
		this.bmi = bmi;
	}



	public User getUser() {
		return user;
	}



	public void setUser(User user) {
		this.user = user;
	}



	public Integer getWeek() {
		return week;
	}



	public void setWeek(Integer week) {
		this.week = week;
	}




	
}
