package com.iBabyApp.model;

import java.io.Serializable;

import java.time.LocalDate;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.springframework.security.crypto.password.PasswordEncoder;

import com.iBabyApp.converter.LocalDateTimeConverter;

@Entity
@Table(name="user")

public class User implements Serializable { 
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="id")
    private long id;  
	
	@Column(name="name")
    private String name;
	
	@Column(name="surname")	
	private String surname;
	
	@Column(name="residence_town")
    private String residenceTown;

	@Column(name="email")	
	private String email;
	

	@Column(name="conceptualdate")
	private LocalDate conceptualDate;
	

	@Column(name="password")
	private String password;
	
	@Column(name="user_photo",nullable = true)
	private String imagePath; 



	public User() {
		super();
		// TODO Auto-generated constructor stub
	}


	public long getId() {
		return id;
	}


	public void setId(long id) {
		this.id = id;
	}


	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public String getSurname() {
		return surname;
	}


	public void setSurname(String surname) {
		this.surname = surname;
	}


	public String getResidenceTown() {
		return residenceTown;
	}


	public void setResidenceTown(String residenceTown) {
		this.residenceTown = residenceTown;
	}


	public String getEmail() {
		return email;
	}




	public void setEmail(String email) {
		this.email = email;
	}


	public LocalDate getConceptualDate() {
		return conceptualDate;
	}


	public void setConceptualDate(LocalDate conceptualDate) {
		this.conceptualDate = conceptualDate;
	}


	public String getPassword() {
		return password;
	}


	public void setPassword(String password) {
		this.password = password;
	}


	public String getImagePath() {
		return imagePath;
	}


	public void setImagePath(String photo) {
		this.imagePath = photo;
	}


	public void encodePassword(PasswordEncoder passwordEncoder) {
		this.password = passwordEncoder.encode(this.password);
	}


	

} 