package com.iBabyApp.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "gynecological_tests")

public class GynecologicalTest implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private long id;

	@Column(name = "week")
	private long week;

	@Column(name = "test_title")
	private String test;
	
	@Column(name = "test_details")
	private String testDetails;

	@Column(name = "yourself_info")
	private String yourselfInfo;

	@Column(name = "yourbaby_info")
	private String yourbabyInfo;
	
	@Column(name = "video")
	private String video;
	

	public String getTestDetails() {
		return testDetails;
	}

	public void setTestDetails(String testDetails) {
		this.testDetails = testDetails;
	}

	public String getVideo() {
		return video;
	}

	public void setVideo(String video) {
		this.video = video;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public long getWeek() {
		return week;
	}

	public void setWeek(long week) {
		this.week = week;
	}

	public String getTest() {
		return test;
	}

	public void setTest(String test) {
		this.test = test;
	}

	public String getYourselfInfo() {
		return yourselfInfo;
	}

	public void setYourselfInfo(String yourselfInfo) {
		this.yourselfInfo = yourselfInfo;
	}

	public String getYourbabyInfo() {
		return yourbabyInfo;
	}

	public void setYourbabyInfo(String yourbabyInfo) {
		this.yourbabyInfo = yourbabyInfo;
	}


	
}
