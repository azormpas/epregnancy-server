package com.iBabyApp.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="weight_limit")
public class WeightLimit {

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="id")
    private Integer id;  

	
    @ManyToOne
    @JoinColumn(name="week", insertable = false, updatable = false)
    private Week week;
    
    @Column(name="weight_max")
    private double max;  
    
    @Column(name="weight_min")
    private double min;  
    
    @ManyToOne
    @JoinColumn(name="bmi", insertable = false, updatable = false)
    private Bmi bmi;

	public WeightLimit() {
		super();
		// TODO Auto-generated constructor stub
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Week getWeek() {
		return week;
	}

	public void setWeek(Week week) {
		this.week = week;
	}

	public double getMax() {
		return max;
	}

	public void setMax(double max) {
		this.max = max;
	}

	public double getMin() {
		return min;
	}

	public void setMin(double min) {
		this.min = min;
	}

	public Bmi getBmi() {
		return bmi;
	}

	public void setBmi(Bmi bmi) {
		this.bmi = bmi;
	}
    
    

}
