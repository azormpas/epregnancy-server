package com.iBabyApp.model;

import java.util.Date;
import java.util.List;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;


@Entity
@Table(name="forum_post")

public class ForumPost {

	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="id")
    private long id;  
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="category_id", insertable = true, updatable = false)
    private ForumCategory forumCategory;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="user_id", insertable = true, updatable = false)
	private User user;
	
    @OneToMany(mappedBy = "forumPost", orphanRemoval = true)
    private List<ForumAnswer> forumAnswer;
	
	@Column(name="title")
    private String title;

	@Column(name="content")	
	private String content;
	
	

	/**
	 * The status describes if the post has been deleted or not. 
	 * The 0 value means that is active and the 1 value means that has been deleted.
	 */
	@Column(name="status")
	private boolean status;
	

    @Column(name = "created_at", columnDefinition = "DATETIME")
    @Temporal(TemporalType.TIMESTAMP)
	private Date createdAt;
	
    @Column(name = "updated_at", columnDefinition = "DATETIME")
    @Temporal(TemporalType.TIMESTAMP)
	private Date updatedAt;

	
	public ForumPost() {
		super();
		// TODO Auto-generated constructor stub
	}


	public long getId() {
		return id;
	}


	public void setId(long id) {
		this.id = id;
	}


	public ForumCategory getForumCategory() {
		return forumCategory;
	}


	public void setForumCategory(ForumCategory forumCategory) {
		this.forumCategory = forumCategory;
	}


	public User getUser() {
		return user;
	}


	public void setUser(User user) {
		this.user = user;
	}


	public String getTitle() {
		return title;
	}


	public void setTitle(String title) {
		this.title = title;
	}


	public String getContent() {
		return content;
	}


	public void setContent(String content) {
		this.content = content;
	}


	public boolean isStatus() {
		return status;
	}


	public void setStatus(boolean status) {
		this.status = status;
	}


	public Date getCreatedAt() {
		return createdAt;
	}


	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}


	public Date getUpdatedAt() {
		return updatedAt;
	}


	public void setUpdatedAt(Date updatedAt) {
		this.updatedAt = updatedAt;
	}


	public List<ForumAnswer> getForumAnswer() {
		return forumAnswer;
	}


	public void setForumAnswer(List<ForumAnswer> forumAnswer) {
		this.forumAnswer = forumAnswer;
	}

	
	
	

}
