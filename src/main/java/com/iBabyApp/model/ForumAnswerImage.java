package com.iBabyApp.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="forum_answer_image")
public class ForumAnswerImage {

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="id")
    private long id;  
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="answer_id", insertable = true, updatable = true)
    private ForumAnswer forumAnswer;
	
	@Column(name="path")
    private String path;
	
	

	public ForumAnswerImage() {
		super();
		// TODO Auto-generated constructor stub
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public ForumAnswer getForumAnswer() {
		return forumAnswer;
	}

	public void setForumAnswer(ForumAnswer forumAnswer) {
		this.forumAnswer = forumAnswer;
	}

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}
	
	
}
