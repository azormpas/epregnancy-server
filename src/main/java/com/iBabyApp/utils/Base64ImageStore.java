package com.iBabyApp.utils;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Base64;

/**
 *  Input : Base64 encoded Image
 *  Outpu : Save it as image file (png)
 */
public class Base64ImageStore
{
    public static String storeImage(String encodedBase64Image,long userId) throws IOException
     {
        String saveFilePath = "C:/Users/asimakis/Documents/ibabyApp_user_images/"+userId+".png";
           	 
    	String base64Image = encodedBase64Image.split(",")[1];
    	
        byte[] imageByte = Base64.getDecoder().decode(base64Image);
        
    	FileOutputStream imageOutFile = new FileOutputStream(saveFilePath);
        
        imageOutFile.write(imageByte);
        
        return saveFilePath;
      }
    
    public static String getImage(String imagePath) throws IOException{
    	
    	File file = new File(imagePath);
    	
    	FileInputStream imageInFile = new FileInputStream(file); 
    	
    	byte imageData[] = new byte[(int) file.length()];
    	
    	imageInFile.read(imageData);
    	
    	String base64Image = Base64.getEncoder().encodeToString(imageData);
    	
    	String base64Prefix = "data:image/jpeg;base64,";
    	
    	System.out.println(base64Image);
    	System.out.println(base64Prefix+base64Image);
	
    	return base64Prefix+base64Image;
    }
}