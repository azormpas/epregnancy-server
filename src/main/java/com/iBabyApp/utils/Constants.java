package com.iBabyApp.utils;

public final class Constants {

    private Constants() {
    }

    public static enum MessageType {
        INFORMATION, ERROR
    }

    public static enum MessageCode {
        LOGIN_SUCCESS, 
        LOGOUT_SUCCESS, 
        USER_CREATION_SUCCESS, 
        USER_VERIFICATION_SUCCESS, 
        USER_UPDATE_SUCCESS,
        RECORD_DELETED,
        DATE_NOT_EXIST,
        BMI_STORE_SUCCESS
    }

    public static enum ErrorCode {
        UNEXPECTED_ERROR,
        UNAUTHORIZED,
        FORBIDDEN,
        LOGIN_FAILURE,
        LOGOUT_FAILURE,
        USER_ALREADY_EXISTS,
        USER_VERIFICATION_FAILURE,
        INCORRECT_PASSWORD,
        WRONG_PASSWORD,
        IMAGE_NOT_FOUND,
        
        /**
         * Validation Errors
         */
        VALIDATION_FAILED,
        HEIGHT_IS_NOT_VALID,
        WEEK_IS_NOT_VALID,
        WEIGHT_BEFORE_IS_NOT_VALID,
        WEIGHT_IS_NOT_VALID
    }

}