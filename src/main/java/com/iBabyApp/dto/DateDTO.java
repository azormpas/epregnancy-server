package com.iBabyApp.dto;

public class DateDTO {

	private String currentDate;
	private String nineMonthsAgo;
	
	public DateDTO() {
	
	}
	

	public DateDTO(String currentDate, String nineMonthsAgo) {
		super();
		this.currentDate = currentDate;
		this.nineMonthsAgo = nineMonthsAgo;
	}


	public String getCurrentDate() {
		return currentDate;
	}

	public void setCurrentDate(String currentDate) {
		this.currentDate = currentDate;
	}

	public String getNineMonthsAgo() {
		return nineMonthsAgo;
	}

	public void setNineMonthsAgo(String nineMonthsAgo) {
		this.nineMonthsAgo = nineMonthsAgo;
	}
	
	
	
	
}
