package com.iBabyApp.dto;

import javax.persistence.Column;

public class WeekDTO{

    private int id;  
	
    private int week;
	


	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getWeek() {
		return week;
	}

	public void setWeek(int week) {
		this.week = week;
	}


}