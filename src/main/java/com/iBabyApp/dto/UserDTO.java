package com.iBabyApp.dto;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import org.springframework.security.crypto.password.PasswordEncoder;

public class UserDTO implements Serializable{
	private static final long serialVersionUID = 1L;
	
	private String message;
	
	private String date;
	
    private long id;  
	
    private String name;
	
	private String surname;
    
    private String residenceTown;

	private String email;
	
	private String password;
	
	private long weeks;
	
	private String newPassword; 
	
	private String photo;
	
	private String imagePath;
	
	

	public UserDTO()
	{
	
	}
	
	public UserDTO(String name, String surname, String residenceTown,String date, String email) {
		this.name = name;
		this.surname = surname;
		this.residenceTown = residenceTown;
		this.date = date;
		this.email = email;
	}
	
	public UserDTO(String message) {
		this.message= message;
	}
	


	public UserDTO (String date,String email) {
		this.email = email;
		this.date = date;
		
	}
	
	public UserDTO (String date,long weeks) {
		this.date = date;
		this.weeks = weeks;
		
	}

	public String getMessage() {
		return message;
	}
	

	public String getNewPassword() {
		return newPassword;
	}

	public void setNewPassword(String newPassword) {
		this.newPassword = newPassword;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public long getId() {
		return id;
	}

	public void setId(long userId) {
		this.id = userId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public String getResidenceTown() {
		return residenceTown;
	}

	public void setResidenceTown(String residenceTown) {
		this.residenceTown = residenceTown;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public long getWeeks() {
		return weeks;
	}

	public void setWeeks(long weeks) {
		this.weeks = weeks;
	}
	

	public void encodePassword(PasswordEncoder passwordEncoder) {
		this.password = passwordEncoder.encode(this.password);
	}
	
	public String getEncodePassword(PasswordEncoder passwordEncoder,String password) {
		return passwordEncoder.encode(password);
	}

	public String getPhoto() {
		return photo;
	}

	public void setPhoto(String photo) {
		this.photo = photo;
	}

	public String getImagePath() {
		return imagePath;
	}

	public void setImagePath(String imagePath) {
		this.imagePath = imagePath;
	}


}
