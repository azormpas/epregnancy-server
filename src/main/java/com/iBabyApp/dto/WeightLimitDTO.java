package com.iBabyApp.dto;

import com.iBabyApp.model.Bmi;
import com.iBabyApp.model.Week;

public class WeightLimitDTO {


    private Integer id;  

    private Week week;
    
    private double max;  
    
    private double min;  

    private Bmi bmi;


    
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Week getWeek() {
		return week;
	}

	public void setWeek(Week week) {
		this.week = week;
	}

	public double getMax() {
		return max;
	}

	public void setMax(double max) {
		this.max = max;
	}

	public double getMin() {
		return min;
	}

	public void setMin(double min) {
		this.min = min;
	}

	public Bmi getBmi() {
		return bmi;
	}

	public void setBmi(Bmi bmi) {
		this.bmi = bmi;
	}
}
    