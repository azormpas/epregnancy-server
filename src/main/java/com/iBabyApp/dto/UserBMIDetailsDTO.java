package com.iBabyApp.dto;

import java.util.List;

import com.iBabyApp.model.User;
import com.iBabyApp.model.WeightLimit;

public class UserBMIDetailsDTO {

    private Long id;  

    private boolean twins;
    
    private Double currentWeight;  
    
    private Double weightBefore;  
    
    private Double height; 
    
    private Double bmi; 
    
    private Integer week; 
    
    private User user; 
    
    private List<WeightLimit> weightLimit; 
    


	public Long getId() {
		return id;
	}



	public void setId(Long id) {
		this.id = id;
	}



	public boolean isTwins() {
		return twins;
	}



	public void setTwins(boolean twins) {
		this.twins = twins;
	}





	public Double getCurrentWeight() {
		return currentWeight;
	}



	public void setCurrentWeight(Double currentWeight) {
		this.currentWeight = currentWeight;
	}



	public Double getWeightBefore() {
		return weightBefore;
	}



	public void setWeightBefore(Double weightBefore) {
		this.weightBefore = weightBefore;
	}



	public Double getHeight() {
		return height;
	}



	public void setHeight(Double height) {
		this.height = height;
	}



	public Double getBmi() {
		return bmi;
	}



	public void setBmi(Double bmi) {
		this.bmi = bmi;
	}



	public User getUser() {
		return user;
	}



	public void setUser(User user) {
		this.user = user;
	}



	public List<WeightLimit> getWeightLimit() {
		return weightLimit;
	}



	public void setWeightLimit(List<WeightLimit> weightLimit) {
		this.weightLimit = weightLimit;
	}
	
	public Integer getWeek() {
		return week;
	}



	public void setWeek(Integer week) {
		this.week = week;
	}









	
}
