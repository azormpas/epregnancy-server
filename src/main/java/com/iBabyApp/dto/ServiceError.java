package com.iBabyApp.dto;

import java.io.Serializable;

import com.iBabyApp.utils.Constants.ErrorCode;

public class ServiceError implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8429010260399049889L;

	private ErrorCode code;
	
	private String message;
	
	public ServiceError() {}
	
	public ServiceError(ErrorCode code) {
		this.code = code;
	}
	
	public ServiceError(String message) {
		this.message = message;
	}


	public ErrorCode getCode() {
		return code;
	}

	public ServiceError setCode(ErrorCode code) {
		this.code = code;
		return this;
	}

}
