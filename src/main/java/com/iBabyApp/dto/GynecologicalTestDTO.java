package com.iBabyApp.dto;

import javax.persistence.Column;

public class GynecologicalTestDTO {

	private int id;

	private long week;

	private String test;

	private String yourselfInfo;

	private String yourbabyInfo;
	
	private String testDetails;
	
	private String video;
	
	
	public GynecologicalTestDTO() 
	{
		super();
	}
	
	public GynecologicalTestDTO( long weeks, String test) {
		this.week = weeks;
		this.test = test;
	
	}
	
	public GynecologicalTestDTO(long week, String test,String testDetails, String yourselfInfo, String yourbabyInfo,String video) {
		this.week = week;
		this.test = test;
		this.testDetails = testDetails;
		this.yourselfInfo = yourselfInfo;
		this.yourbabyInfo = yourbabyInfo;
		this.video = video;
	}

	public String getTestDetails() {
		return testDetails;
	}

	public void setTestDetails(String testDetails) {
		this.testDetails = testDetails;
	}

	public String getVideo() {
		return video;
	}

	public void setVideo(String video) {
		this.video = video;
	}

	public void setWeek(long week) {
		this.week = week;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public long getWeek() {
		return week;
	}

	public void setWeek(int weeks) {
		this.week = weeks;
	}

	public String getTest() {
		return test;
	}

	public void setTest(String test) {
		this.test = test;
	}

	public String getYourselfInfo() {
		return yourselfInfo;
	}

	public void setYourselfInfo(String yourselfInfo) {
		this.yourselfInfo = yourselfInfo;
	}

	public String getYourbabyInfo() {
		return yourbabyInfo;
	}

	public void setYourbabyInfo(String yourbabyInfo) {
		this.yourbabyInfo = yourbabyInfo;
	}

}
