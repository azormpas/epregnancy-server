package com.iBabyApp.service;

import java.util.ArrayList;
import java.util.List;

import org.dozer.DozerBeanMapper;
import org.dozer.Mapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.iBabyApp.dao.WeekDAO;
import com.iBabyApp.dto.WeekDTO;
import com.iBabyApp.model.Week;
import com.iBabyApp.utils.ServiceException;

@Service
public class WeekService {
	
	@Autowired
	private WeekDAO weekDAO;
	
    static Mapper mapper = new DozerBeanMapper();

	
	public List<WeekDTO> getListByWeeks()  throws ServiceException {

		List<Week> weekList = weekDAO.getWeekList();
		List<WeekDTO> weekListDTO = new ArrayList<WeekDTO>(); 
		   for (Week w : weekList) {
			   weekListDTO.add(mapper.map(w, WeekDTO.class));
		    }
			
		return weekListDTO;
	}

}
