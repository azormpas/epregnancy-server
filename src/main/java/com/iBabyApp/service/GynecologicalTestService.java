package com.iBabyApp.service;


import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.iBabyApp.model.GynecologicalTest;
import com.iBabyApp.dao.GynecologicalTestDAO;
import com.iBabyApp.dto.GynecologicalTestDTO;

@Service
public class GynecologicalTestService {
	
	
	@Autowired
	private GynecologicalTestDAO gynecologicalTestDAO;
	
	
	public ArrayList<GynecologicalTestDTO> getUserTest(String weeks){
		
		
		long week = Long.parseLong(weeks);
		ArrayList<GynecologicalTestDTO> listOfTests = new ArrayList<GynecologicalTestDTO>();
		
		if(week <=38) {
		
		GynecologicalTest gynecologicalTest1 =  gynecologicalTestDAO.nextTest(week);
		GynecologicalTestDTO gynecologicalDTO1= new  GynecologicalTestDTO(gynecologicalTest1.getWeek(),gynecologicalTest1.getTest());
		
		week++;
		
		GynecologicalTest gynecologicalTest2 =  gynecologicalTestDAO.nextTest(week);
		GynecologicalTestDTO gynecologicalDTO2= new  GynecologicalTestDTO(gynecologicalTest2.getWeek(),gynecologicalTest2.getTest());
		
		week++;
		
		GynecologicalTest gynecologicalTest3 =  gynecologicalTestDAO.nextTest(week);
		GynecologicalTestDTO gynecologicalDTO3= new  GynecologicalTestDTO(gynecologicalTest3.getWeek(),gynecologicalTest3.getTest());
		
		
		listOfTests.add(gynecologicalDTO1);
		listOfTests.add(gynecologicalDTO2);
		listOfTests.add(gynecologicalDTO3);
		
		}
		
		else if(week == 39) {
		GynecologicalTest gynecologicalTest1 =  gynecologicalTestDAO.nextTest(week);
		GynecologicalTestDTO gynecologicalDTO1= new  GynecologicalTestDTO(gynecologicalTest1.getWeek(),gynecologicalTest1.getTest());
			
		week++;
			
		GynecologicalTest gynecologicalTest2 =  gynecologicalTestDAO.nextTest(week);
		GynecologicalTestDTO gynecologicalDTO2= new  GynecologicalTestDTO(gynecologicalTest2.getWeek(),gynecologicalTest2.getTest());
		
		listOfTests.add(gynecologicalDTO1);
		listOfTests.add(gynecologicalDTO2);
		}
		
		else if(week >= 40) {
			
		GynecologicalTest gynecologicalTest1 =  gynecologicalTestDAO.nextTest(week);
		GynecologicalTestDTO gynecologicalDTO1= new  GynecologicalTestDTO(gynecologicalTest1.getWeek(),gynecologicalTest1.getTest());	
		listOfTests.add(gynecologicalDTO1);
			
		}
	
		return listOfTests;
	}
	
	
	
	public GynecologicalTestDTO getWeeklyExamination(String weeks){
		
		long week = Long.parseLong(weeks);
		
		GynecologicalTest gynecologicalTest =  gynecologicalTestDAO.nextTest(week);
		GynecologicalTestDTO gynecologicalDTO= new  GynecologicalTestDTO(gynecologicalTest.getWeek(),gynecologicalTest.getTest(),gynecologicalTest.getTestDetails(),gynecologicalTest.getYourselfInfo(),gynecologicalTest.getYourbabyInfo(),gynecologicalTest.getVideo());
		
		return gynecologicalDTO;
	}


}



	