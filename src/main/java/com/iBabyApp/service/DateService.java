package com.iBabyApp.service;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.iBabyApp.dao.UserDAO;
import com.iBabyApp.dto.DateDTO;
import com.iBabyApp.dto.UserDTO;
import com.iBabyApp.model.User;
import com.iBabyApp.utils.ServiceException;

@Service
public class DateService {
	
	@Autowired
	private UserDAO userDAO;
	
	public UserDTO dateExist(String email) {
		
		User user = userDAO.findUser(email);
		
		if(user.getConceptualDate()!= null) {
	   		
			DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
			
			//convert String to LocalDate

			LocalDate currentDay = LocalDate.now();
			
			LocalDate birthDate = user.getConceptualDate().plus(280, ChronoUnit.DAYS);
			
			long weeks = ChronoUnit.WEEKS.between(user.getConceptualDate(), currentDay);
			
			if(weeks > 40 ){
				weeks = 40;
			}

			String date = birthDate.format(formatter);
			
			UserDTO userDTO = new UserDTO(date,weeks);
  		
			return userDTO ;
		}
		else 
		{
		    UserDTO notFoundMessage = new UserDTO("NOT EXIST");
			return notFoundMessage;
		}
	}
	
	public DateDTO datetimeDropDown() {
		
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
		
		//convert String to LocalDate

		LocalDate currentDay = LocalDate.now();
		
		LocalDate nineMonth = currentDay.plus(-280, ChronoUnit.DAYS);
		
		String nineMonthsAgo = nineMonth.format(formatter);
		String today = currentDay.format(formatter);
		
		DateDTO dateDTO = new DateDTO(today,nineMonthsAgo);
		
		return dateDTO;
		
	}
	
	public String saveDate(String date, String email) throws ServiceException {

		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
		LocalDate dateTime = LocalDate.parse(date);
		User user = userDAO.findUser(email);
		user.setConceptualDate(dateTime);
		userDAO.update(user);;
		
		String formatDate = formatter.format(user.getConceptualDate());
		return formatDate;
		
}
	

}
