package com.iBabyApp.service;

import java.io.IOException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import org.dozer.DozerBeanMapper;
import org.dozer.Mapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import com.iBabyApp.dao.UserDAO;
import com.iBabyApp.model.User;
import com.iBabyApp.utils.Base64ImageStore;
import com.iBabyApp.utils.Constants.ErrorCode;
import com.iBabyApp.utils.ServiceException;
import com.iBabyApp.dto.UserDTO;

@Service
public class UserService{
	
	@Autowired
	private UserDAO userDAO;
	
	@Autowired
    DozerBeanMapper dozerBeanMapper;
	
	@Autowired
	private PasswordEncoder passwordEncoder;
	
	private static Base64ImageStore Base64ImageStore;

    static Mapper mapper = new DozerBeanMapper();
	

	
	public void createUser(User user)  throws ServiceException {

		if (userDAO.userExists(user.getEmail())) {
			throw new ServiceException(ErrorCode.USER_ALREADY_EXISTS);
		} else {
			user.encodePassword(this.passwordEncoder);
			userDAO.create(user);
		}
	}


	
	public UserDTO getUserProfileData(String email) throws ServiceException {
		
		try{
		    User user = userDAO.findUser(email);
		  //  System.out.println(user.getImagePath());
		    String photo = "";
		    if(user.getImagePath() != null){
		    	photo = Base64ImageStore.getImage(user.getImagePath());
		    }
		    else{
		    	photo = null;
		    }
		    
			UserDTO usr = dozerBeanMapper.map(user, UserDTO.class);
			usr.setPhoto(photo);		
			return usr ;		
		}
		catch(IOException e) {
			e.printStackTrace();
			throw new ServiceException(ErrorCode.IMAGE_NOT_FOUND);
	}
		
		
	}
	
	public void updateProfileData(String email,UserDTO userDTO) throws ServiceException {
		   
	 try{
		   User user = userDAO.findUser(email);
			   
		   DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
		   LocalDate dateTime = LocalDate.parse(userDTO.getDate(),formatter);
		   
		   
		   if(userDTO.getPassword() != null && passwordEncoder.matches(userDTO.getPassword(), user.getPassword())){
			   System.out.print("Password Changed");
			   userDTO.setPassword(userDTO.getNewPassword());
			   userDTO.encodePassword(this.passwordEncoder);
		   }
		   else if(userDTO.getPassword() != null && !passwordEncoder.matches(userDTO.getPassword(), user.getPassword())){
			   System.out.print("Wrong Password");
				throw new ServiceException(ErrorCode.WRONG_PASSWORD);
		   }
		   else if(userDTO.getPassword() == null){
			   userDTO.setPassword(user.getPassword());
		   }
		   
		   if(userDTO.getPhoto()!= null){
			   String imagePath = Base64ImageStore.storeImage(userDTO.getPhoto(),user.getId());
			   userDTO.setImagePath(imagePath);
		   }
		  
		   dozerBeanMapper.map(userDTO, user);
		   user.setConceptualDate(dateTime);		   
		   userDAO.update(user);
	 }
		catch(IOException e) {
			e.printStackTrace();
			throw new ServiceException(ErrorCode.IMAGE_NOT_FOUND);
	}   
		
	}
	
	

	
} 