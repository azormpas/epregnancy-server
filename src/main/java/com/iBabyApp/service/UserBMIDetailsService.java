package com.iBabyApp.service;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.NoResultException;

import org.dozer.DozerBeanMapper;
import org.dozer.Mapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.MethodArgumentNotValidException;

import com.iBabyApp.dao.UserBMIDetailsDAO;
import com.iBabyApp.dao.UserDAO;
import com.iBabyApp.dao.WeightLimitDAO;
import com.iBabyApp.dto.UserBMIDetailsDTO;
import com.iBabyApp.model.UserBMIDetails;
import com.iBabyApp.model.WeightLimit;
import com.iBabyApp.utils.Constants;
import com.iBabyApp.utils.ServiceException;
import com.iBabyApp.validation.WeightChartValidation;

@Service
public class UserBMIDetailsService {

	@Autowired
	private WeightLimitDAO weightLimitDAO;

	@Autowired
	private UserBMIDetailsDAO userBMIDetailsDAO;

	@Autowired
	private UserDAO userDAO;

	@Autowired
	private DozerBeanMapper dozerBeanMapper;


	/**
	 * 
	 * @param userBMIDetailsDTO contains the user's details (height,bmi,current weight,weight limit list,week etc) that will be stored
	 * @param email
	 * @return a UserBMIDetailsDTO which contains the user's details for the Weight chart
	 */
	public UserBMIDetailsDTO saveUserBMI(UserBMIDetailsDTO userBMIDetailsDTO, String email) throws ServiceException {

		// save if it is the first time that the user stores her
		// (BMI/height/week etc) details
		if (userBMIDetailsDTO.getId() == null) {
			UserBMIDetails u = dozerBeanMapper.map(userBMIDetailsDTO, UserBMIDetails.class);
			u.setUser(userDAO.findUser(email));
			userBMIDetailsDAO.create(u);
			userBMIDetailsDTO.setWeightLimit(getWeightLimit(userBMIDetailsDTO.getBmi(), userBMIDetailsDTO.isTwins()));
			userBMIDetailsDTO.setId(u.getId());
			return userBMIDetailsDTO;
		}
		// update if it is not the first time that user stores her details
		else {
			UserBMIDetails uBMI = userBMIDetailsDAO.find(userBMIDetailsDTO.getId());
			uBMI = dozerBeanMapper.map(userBMIDetailsDTO, UserBMIDetails.class);
			userBMIDetailsDAO.update(uBMI);
			// set the weight limit list
			userBMIDetailsDTO.setWeightLimit(getWeightLimit(userBMIDetailsDTO.getBmi(), userBMIDetailsDTO.isTwins()));
			return userBMIDetailsDTO;
		}

	}
	
	/**
	 * @param email
	 * @return the user's details (height,bmi,current weight,weight limit list,week etc)
	 */
	
	public UserBMIDetailsDTO getUserBMI(String email) {
		try{
			UserBMIDetailsDTO u = dozerBeanMapper.map(userBMIDetailsDAO.getUserBMIDetails(email),UserBMIDetailsDTO.class);
			u.setWeightLimit(getWeightLimit(u.getBmi(), u.isTwins()));
			return u;
		}
		catch(NoResultException nre){
			UserBMIDetailsDTO u = new UserBMIDetailsDTO();
			return u;
		}

	}
	
	/**
	 * @param bmi contains the user's BMI
	 * @param twins
	 * @return a list that contains the maximum and minimum weight that user can gain during her pregnancy
	 */

	public List<WeightLimit> getWeightLimit(double bmi, boolean twins) {

		List<WeightLimit> weightLimitList = new ArrayList<WeightLimit>();

		if (bmi < 18.5 && !twins) {
			weightLimitList = weightLimitDAO.getWeightLimitList(1);
		} else if (bmi >= 18.5 && bmi <= 24.9 && !twins) {
			weightLimitList = weightLimitDAO.getWeightLimitList(2);
		} else if (bmi >= 25.0 && bmi <= 29.9 && !twins) {
			weightLimitList = weightLimitDAO.getWeightLimitList(3);
		} else if (bmi >= 30 && !twins) {
			weightLimitList = weightLimitDAO.getWeightLimitList(4);
		} else if (bmi < 18.5 && twins) {
			weightLimitList = weightLimitDAO.getWeightLimitList(5);
		} else if (bmi >= 18.5 && bmi <= 24.9 && twins) {
			weightLimitList = weightLimitDAO.getWeightLimitList(6);
		} else if (bmi >= 25.0 && bmi <= 29.9 && twins) {
			weightLimitList = weightLimitDAO.getWeightLimitList(7);
		} else if (bmi >= 30 && twins) {
			weightLimitList = weightLimitDAO.getWeightLimitList(8);
		}
		return weightLimitList;
	}

}
