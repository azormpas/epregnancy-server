package com.iBabyApp.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;

import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.iBabyApp.dto.UserBMIDetailsDTO;
import com.iBabyApp.model.UserBMIDetails;
import com.iBabyApp.model.Week;
import com.iBabyApp.model.WeightLimit;


@Transactional
@Repository
public class UserBMIDetailsDAO extends GenericRepository<UserBMIDetails>{


	@PersistenceContext	
	private EntityManager entityManager;
	
	public UserBMIDetails getUserBMIDetails(String email) throws NoResultException{

		String query = "FROM UserBMIDetails as u WHERE  u.user.email =:email";
		UserBMIDetails userBMIDetails = (UserBMIDetails) entityManager.createQuery(query).setParameter("email", email).getSingleResult();
		return userBMIDetails;
	
	}
	
	
	public UserBMIDetails find(Long id) {
		
		String query = "FROM UserBMIDetails as u WHERE  id =:id";

		UserBMIDetails userBMIDetails = (UserBMIDetails) entityManager.createQuery(query).setParameter("id", id).getSingleResult();
	
		return  userBMIDetails;
	}

}
