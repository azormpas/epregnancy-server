package com.iBabyApp.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import com.iBabyApp.model.Week;


@Transactional
@Repository
public class WeekDAO extends GenericRepository<Week>{


	@PersistenceContext	
	private EntityManager entityManager;
	
	public List<Week> getWeekList(){
		
		String query = "SELECT w FROM Week w";
		List<Week> weekList =  (List<Week>) entityManager.createQuery(query).getResultList();
		return weekList;
	}

}
