package com.iBabyApp.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.iBabyApp.model.WeightLimit;



@Transactional
@Repository
public class WeightLimitDAO extends GenericRepository<WeightLimit>{

	@PersistenceContext	
	private EntityManager entityManager;
	
	public List<WeightLimit> getWeightLimitList(int bmi){
		
		String query = "FROM WeightLimit w WHERE  w.bmi.id =:bmi";
		List<WeightLimit> weightLimitList =  (List<WeightLimit>) entityManager.createQuery(query).setParameter("bmi", bmi).getResultList();
		return weightLimitList;
	}
}
