package com.iBabyApp.dao;


import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.iBabyApp.model.User;

@Transactional
@Repository
public class UserDAO extends GenericRepository<User> {
	
	@PersistenceContext	
	private EntityManager entityManager;
	
	
	public User getUserById(long id) {
		
		return entityManager.find(User.class, id);
	}
	

	public User findUser(String email) {
		String hql = "FROM User as u WHERE  u.email =:email";

		User user = (User) entityManager.createQuery(hql).setParameter("email", email).getSingleResult();
	
		return   user;
	}

	
	public boolean userExists(String email) {
		
		String query = "FROM User as u WHERE  u.email =:email";
		int count =  (int) entityManager.createQuery(query).setParameter("email", email).getResultList().size();
		if(count >0) 
		{
			return true;
		}
		else {
			return false;
		}	
	}

}
