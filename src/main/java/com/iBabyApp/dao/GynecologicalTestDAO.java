package com.iBabyApp.dao;

import com.iBabyApp.model.GynecologicalTest;
import com.iBabyApp.model.User;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;



@Transactional
@Repository
public class GynecologicalTestDAO extends GenericRepository<GynecologicalTest>{


	@PersistenceContext	
	private EntityManager entityManagers;
	
	
	public GynecologicalTest nextTest (long week) {
		
		System.out.println("den trexei");
		String hql = "FROM GynecologicalTest as g WHERE  g.week =:week";

		GynecologicalTest gynecologicalTest = (GynecologicalTest) entityManagers.createQuery(hql).setParameter("week", week).getSingleResult();
		
		return gynecologicalTest;
	}
}
