package com.iBabyApp.dao;

import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.validation.UnexpectedTypeException;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.TransactionSystemException;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.MethodArgumentNotValidException;

import com.iBabyApp.annotation.SayHello;

@Transactional
@Repository
public class GenericRepository<T> {
	
	@PersistenceContext	
	private EntityManager entityManager;

	public void create(T t) {
		entityManager.persist(t);
	}


	public void update(T t) {
		entityManager.merge(t);
	}
	
	public void delete(T t) {
		entityManager.remove(t);
	}
	

 
}