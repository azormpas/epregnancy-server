package com.iBabyApp.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.iBabyApp.dto.WeekDTO;
import com.iBabyApp.service.WeekService;
import com.iBabyApp.utils.ServiceException;

@CrossOrigin
@Controller
@RequestMapping("/week")


public class WeekController {
	
	@Autowired
	private WeekService weekService;
	
	@GetMapping("/list")
	public ResponseEntity<List<WeekDTO>> getWeekList () throws ServiceException {
		
		return new ResponseEntity <List<WeekDTO>> (weekService.getListByWeeks(),HttpStatus.OK);
	}
	


}