package com.iBabyApp.controller;




import java.text.ParseException;


import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.AuthenticationException;
import com.iBabyApp.IBabyAppApplication;
import com.iBabyApp.model.User;
import com.iBabyApp.security.jwt.TokenProvider;

@CrossOrigin
@Controller
@RequestMapping("/authentication")
public class LoginController {
	
	
	@Autowired
	private TokenProvider tokenProvider;

	@Autowired
	private AuthenticationManager authenticationManager;


	
	 @PostMapping("/login")
		public ResponseEntity<String> authorize(@Valid @RequestBody User loginUser,HttpServletResponse response) 
		{
			UsernamePasswordAuthenticationToken authenticationToken = new UsernamePasswordAuthenticationToken(
					loginUser.getEmail(), loginUser.getPassword());
			
			try {
				this.authenticationManager.authenticate(authenticationToken);
				String token = tokenProvider.createToken(loginUser.getEmail());
				System.out.println(token);
				return new ResponseEntity<String>(token, HttpStatus.CREATED);
			}
			catch (AuthenticationException e) {
				IBabyAppApplication.logger.info("Security exception {}", e.getMessage());
				response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
				return null;
			}
		}
	

} 