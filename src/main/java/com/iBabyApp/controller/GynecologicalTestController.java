package com.iBabyApp.controller;

import java.util.ArrayList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import org.springframework.web.bind.annotation.RequestMapping;
import com.iBabyApp.dto.GynecologicalTestDTO;
import com.iBabyApp.service.GynecologicalTestService;

@CrossOrigin
@Controller
@RequestMapping("/gynecologicalTest")


public class GynecologicalTestController {
	
	@Autowired
	private GynecologicalTestService gynecologicalService;
	
	@GetMapping("/testList/{week}")
	public ResponseEntity<ArrayList<GynecologicalTestDTO>> gynecologicalTestTitle (@PathVariable String week) {
		
		return new ResponseEntity <ArrayList<GynecologicalTestDTO>> (gynecologicalService.getUserTest(week),HttpStatus.OK);
	}
	
	
	@GetMapping("/weeklyExamination/{week}")
	public ResponseEntity<GynecologicalTestDTO> gynecologicalTWeeklyExamination (@PathVariable String week) {
	
		return new ResponseEntity <GynecologicalTestDTO> (gynecologicalService.getWeeklyExamination(week),HttpStatus.OK);
	}

}
