package com.iBabyApp.controller;


import java.security.Principal;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import com.iBabyApp.model.User;
import com.iBabyApp.service.UserService;
import com.iBabyApp.utils.ServiceException;
import com.iBabyApp.utils.Constants.MessageCode;
import com.iBabyApp.dto.ServiceMessage;
import com.iBabyApp.dto.UserDTO;

@CrossOrigin
@Controller
@RequestMapping("/user")
public class UserController {
	
	@Autowired
	private UserService userService;
	
	
	@PostMapping("/signup")
	public ResponseEntity<ServiceMessage> signup(@RequestBody User signupUser) throws ServiceException{
		userService.createUser(signupUser);		
		ServiceMessage se = new ServiceMessage(MessageCode.USER_CREATION_SUCCESS);
		return new ResponseEntity<ServiceMessage>(se,HttpStatus.CREATED);
	}
	

	@PostMapping("/loadProfileData")
	public ResponseEntity<UserDTO> sendUserProfileData (HttpServletRequest request) throws ServiceException {
		Principal principal = request.getUserPrincipal();
		return new ResponseEntity<UserDTO>(userService.getUserProfileData(principal.getName()),HttpStatus.OK);
	}
	
	@PostMapping("/updateProfileData")
	public ResponseEntity<ServiceMessage> updateUserProfileData (@RequestBody UserDTO user,HttpServletRequest request) throws ServiceException{
		Principal principal = request.getUserPrincipal();
		userService.updateProfileData(principal.getName(), user);
		ServiceMessage se = new ServiceMessage(MessageCode.USER_UPDATE_SUCCESS);
		return new ResponseEntity<ServiceMessage>(se,HttpStatus.OK);
	}
	

	

} 