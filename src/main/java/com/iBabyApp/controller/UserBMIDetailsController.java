
package com.iBabyApp.controller;

import java.security.Principal;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import com.iBabyApp.dto.UserBMIDetailsDTO;
import com.iBabyApp.service.UserBMIDetailsService;
import com.iBabyApp.utils.ServiceException;
import com.iBabyApp.validation.WeightChartValidation;

@CrossOrigin
@Controller
@RequestMapping("/chart")


public class UserBMIDetailsController {

	@Autowired
	private UserBMIDetailsService userBMIDetailsService;
	
	@PostMapping("/save/bmi")
	public ResponseEntity<UserBMIDetailsDTO> saveBmi (@Valid @RequestBody UserBMIDetailsDTO u,HttpServletRequest request) throws ServiceException{
		Principal principal = request.getUserPrincipal();
		WeightChartValidation.isValid(u);
		return new ResponseEntity <UserBMIDetailsDTO>(userBMIDetailsService.saveUserBMI(u, principal.getName()),HttpStatus.OK);
	}
	
	@GetMapping("/get/bmi")
	public ResponseEntity<UserBMIDetailsDTO> getBmi (HttpServletRequest request) {
		Principal principal = request.getUserPrincipal();
		return new ResponseEntity <UserBMIDetailsDTO>(userBMIDetailsService.getUserBMI(principal.getName()),HttpStatus.OK);
	}
	
	
	
}