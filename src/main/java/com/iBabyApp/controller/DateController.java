package com.iBabyApp.controller;


import java.security.Principal;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import com.iBabyApp.dto.DateDTO;
import com.iBabyApp.dto.UserDTO;
import com.iBabyApp.service.DateService;
import com.iBabyApp.utils.ServiceException;

@CrossOrigin
@Controller
@RequestMapping("/calendar")


public class DateController {
	
	@Autowired
	private DateService dateService;
	
	
	@GetMapping("/date")
	public ResponseEntity<UserDTO> checkDate (HttpServletRequest request) {
		//email
		Principal principal = request.getUserPrincipal();
		System.out.println("##############");
		System.out.println(principal.getName());
		UserDTO userDTO = dateService.dateExist(principal.getName());
		return new ResponseEntity<UserDTO>(userDTO,HttpStatus.OK);
	}
	

	@PostMapping("/save/date")
	public ResponseEntity<UserDTO> saveDate (@RequestBody UserDTO user,HttpServletRequest request)  throws ServiceException {
		Principal principal = request.getUserPrincipal();
		String date = dateService.saveDate(user.getDate(),principal.getName());
		UserDTO userDTO = new UserDTO(date);
		return new ResponseEntity<UserDTO>(userDTO,HttpStatus.CREATED);
	
	}
	
	@GetMapping("/dropdownDate")
	public ResponseEntity<DateDTO> dropdownDate () {
		return new ResponseEntity<DateDTO>(dateService.datetimeDropDown(),HttpStatus.OK);
	}

}
