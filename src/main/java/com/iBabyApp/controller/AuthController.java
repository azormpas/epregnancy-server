package com.iBabyApp.controller;


import org.springframework.security.authentication.AuthenticationManager;

import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import com.iBabyApp.security.jwt.TokenProvider;
import com.iBabyApp.service.UserService;


@CrossOrigin
@RestController
public class AuthController {

	private  UserService userService;

	private TokenProvider tokenProvider;

	private PasswordEncoder passwordEncoder;

	private AuthenticationManager authenticationManager;

	public AuthController(PasswordEncoder passwordEncoder, UserService userService,
			TokenProvider tokenProvider, AuthenticationManager authenticationManager) {
	}
	
    @GetMapping("/authenticate")
    public void authenticate() {
    	// we don't have to do anything here
    	// this is just a secure endpoint and the JWTFilter
    	// validates the token
    	// this service is called at startup of the app to check 
    	// if the jwt token is still valid
    }
    


}
