package com.iBabyApp.validation;

import com.iBabyApp.dto.UserBMIDetailsDTO;
import com.iBabyApp.utils.ServiceException;
import com.iBabyApp.utils.Constants.ErrorCode;

public class WeightChartValidation {

	
	public static void isValid(UserBMIDetailsDTO u) throws ServiceException{
		
		if(u.getHeight() == null || u.getHeight() < 0 || u.getHeight() > 10000){
			throw new ServiceException(ErrorCode.HEIGHT_IS_NOT_VALID);
		}
		else if(u.getCurrentWeight() == null || u.getCurrentWeight() < 0 || u.getCurrentWeight() >  10000){
			throw new ServiceException(ErrorCode.WEIGHT_IS_NOT_VALID);
		}
		else if(u.getWeightBefore() == null || u.getWeightBefore() < 0 || u.getWeightBefore() > 10000){
			throw new ServiceException(ErrorCode.WEIGHT_BEFORE_IS_NOT_VALID);
		}
		else if(u.getWeek() == null || u.getWeek() > 40 || u.getWeek() < 0){
			throw new ServiceException(ErrorCode.WEEK_IS_NOT_VALID);
		}

	}
}
